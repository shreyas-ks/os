#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <pthread.h>
#include <string.h>
#include <sys/time.h>
#include <errno.h>
#include <math.h>
#include <sys/stat.h>
#include <unistd.h>
#include "my402list.h"

#define queue_thread_no 0
#define token_thread_no 1
#define server1_thread_no 2
#define server2_thread_no 3
#define signal_thread_no 4

pthread_t thread[5];

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond_var = PTHREAD_COND_INITIALIZER;

struct node{
	double q1_arrive;
	double q1_start;
	double q1_end;
	int token_required;
	double q2_start;
	double q2_end;
	double lambda;
 	double service_rate;
 	double service_time;
 	double service_time_real;
	double inter_arrival_time;
	double inter_arrival_time_real;
	double s1_start;
	double s1_end;
	double s2_start;
	double s2_end;
	int id;
};
typedef struct node packet;

My402List listq1;
My402List listq2;
sigset_t set;

struct node1{
	double arrival_rate;
	double arrival_time;
	double arrival_time_real;
	int id;
};
typedef struct node1 token;

struct node2{
	int packets_s1;
	int packets_s2;
	int time_s1;
	int time_s2;
	int s1_ready;
	int s2_ready;
};
typedef struct node2 server_node;

server_node server;

int thread_done=0; 
int packet_inter_arrival_time=0;
int packets_q1=0;
int packets_dropped=0;
int packets_accepted=0;
int token_count=0;
double time_in_q1=0;
double time_in_q2=0;
int cntrl_c_pressed=0;
int token_total=0;
int token_drop=0;
int number_of_packets=0;
int t_flag=0;
int token_bucket_height=0;
int total_system_time=0;
int total_system_time_sq=0;
char tsfile[20];
double start_time;
FILE *fp=NULL;

void copy_packet(packet *packet_cur,packet* packet_pre){
	packet_cur->q1_arrive=packet_pre->q1_arrive;
	packet_cur->q1_start=packet_pre->q1_start;
	packet_cur->q1_end=packet_pre->q1_end;
	packet_cur->token_required=packet_pre->token_required;
	packet_cur->q2_start=packet_pre->q2_start;
	packet_cur->q2_end=packet_pre->q2_end;
	packet_cur->lambda=packet_pre->lambda;
 	packet_cur->service_rate=packet_pre->service_rate;
 	packet_cur->service_time=packet_pre->service_time;
	packet_cur->inter_arrival_time=packet_pre->inter_arrival_time;
	packet_cur->s1_start=packet_pre->s1_start;
	packet_cur->s1_end=packet_pre->s1_end;
	packet_cur->s2_start=packet_pre->s2_start;
	packet_cur->s2_end=packet_pre->s2_end;
	packet_cur->id=packet_pre->id;
}
double current_time(){
	struct timeval current_time;
	gettimeofday(&current_time,NULL);
	return ((double)(current_time.tv_sec*1000+(double)current_time.tv_usec/1000));
}

void display_parameters(packet *packet, token * token){
		if(number_of_packets==0){
				fprintf(stderr,"Error: Token bucket size can't be zero\n");
				exit(1);	
		}
		if(token_bucket_height==0){
				fprintf(stderr, "Error: Token bucket size can't be zero\n");
				exit(1);
		}
		if(packet->lambda ==0 ){
				fprintf(stderr, "Error: lambda can't be zero\n");
				exit(1);
		}
		if(packet->service_rate ==0 ){
				fprintf(stderr, "Error: Service rate can't be zero\n");
				exit(1);
		}
		if(token->arrival_rate ==0 ){
				fprintf(stderr, "Error: Arrival rate can't be zero\n");
				exit(1);
		}
		if(packet->token_required ==0 ){
				fprintf(stderr, "Error: No. of tokens required can't be zero\n");
				exit(1);
		}
		fprintf(stdout,"Emulation Parameters:\n");
		fprintf(stdout,"\tnumber to arrive = %d\n",number_of_packets);
		if(t_flag==0){
			fprintf(stdout,"\tlambda = %.6g\n",packet->lambda);
			fprintf(stdout,"\tmu = %.6g\n",packet->service_rate);
		}
		fprintf(stdout,"\tr = %.6g\n",token->arrival_rate);
		fprintf(stdout,"\tB = %d\n",token_bucket_height);
		if(t_flag==0){
			fprintf(stdout,"\tP = %d\n",packet->token_required);
		}
		if(t_flag==1){
			fprintf(stdout,"\ttsfile = %s\n\n",tsfile);
		}
		if(packet->lambda < 0.1){
				packet->lambda=0.1;
		}
		if(packet->service_rate < 0.1){
				packet->service_rate=0.1;
		}
		if(token->arrival_rate < 0.1){
			token->arrival_rate=0.1;
		}
}

void display_statistics(){
	int length_q1;
	int length_q2;
	int i;
	length_q1=My402ListLength(&listq1);
	length_q2=My402ListLength(&listq2);
	My402ListElem temp;
	for(i=0;i<length_q2;i++){
		temp.obj=listq2.anchor.prev->obj;
		fprintf(stdout,"%012.3lfms: p%d removed from Q1\n",current_time()-start_time,((packet*)temp.obj)->id);
		My402ListUnlink(&listq2,listq2.anchor.prev);
	}
	for(i=0;i<length_q1;i++){
		temp.obj=listq1.anchor.prev->obj;
		fprintf(stdout,"%012.3lfms: p%d removed from Q1\n",current_time()-start_time,((packet*)temp.obj)->id);
		My402ListUnlink(&listq1,listq1.anchor.prev);
	}
	double total_simulation_time = current_time()-start_time;
	fprintf(stdout,"%012.3lfms: emulation ends\n\n",total_simulation_time);
		fprintf(stdout,"Statistics:\n\n");
	if(packets_q1==0){
		fprintf(stdout,"average packet inter-arrival time = N/A\n, No packets received at Q1\n");
	}
	else{
		fprintf(stdout,"\taverage packet inter-arrival time = %.6g\n",(packet_inter_arrival_time/(double)packets_q1)/1000.0);
	}
	if((server.packets_s1 + server.packets_s2)==0){
		fprintf(stdout,"\taverage packet service time = N/A, No packets have been served in either servers\n\n");
	}
	else{
		fprintf(stdout,"\taverage packet service time = %.6g\n\n",((server.time_s1 +server.time_s2)/((double)server.packets_s2+(double)server.packets_s1))/1000.0);
	}
	if(total_simulation_time==0){
		fprintf(stdout,"\taverage number of packets in Q1 =N/A, Simulation didn't run\n");
		fprintf(stdout,"\taverage number of packets in Q2 =N/A, Simulation didn't run\n");
		fprintf(stdout,"\taverage number of packets in S1 =N/A, Simulation didn't run\n");
		fprintf(stdout,"\taverage number of packets in S2 =N/A, Simulation didn't run\n\n");
	}
	else{
		fprintf(stdout,"\taverage number of packets in Q1 = %.6g\n",time_in_q1/total_simulation_time);
		fprintf(stdout,"\taverage number of packets in Q2 = %.6g\n",time_in_q2/total_simulation_time);
		fprintf(stdout,"\taverage number of packets at S1 = %.6g\n",server.time_s1/total_simulation_time);
		fprintf(stdout,"\taverage number of packets at S2 = %.6g\n\n",server.time_s2/total_simulation_time);
	}
	if((server.packets_s1+server.packets_s2)==0){
		fprintf(stdout,"\taverage time a packet spent in system = N/A, No packets have been served by either servers\n");
	}
	else{
		fprintf(stdout,"\taverage time a packet spent in system = %.6g\n",(total_system_time/((double)server.packets_s1+(double)server.packets_s1))/1000.0);
	}
	if((server.packets_s1+server.packets_s2)==0){
		fprintf(stdout,"\tstandard deviation for time spent in system =  N/A, No packets have been served by either servers\n");
	}
	else{
		double mean=total_system_time/((double)server.packets_s1+(double)server.packets_s2);
		mean*=mean;
		double temp=total_system_time_sq/((double)server.packets_s1+(double)server.packets_s2);
		fprintf(stdout,"\tstandard deviation for time spent in system = %.6g\n",(sqrt(abs(mean-temp)))/1000.0);
	}
	if(token_total==0){
		fprintf(stdout,"\ttoken drop probability = N/A, No tokens in the system\n");
	}
	else{
		fprintf(stdout,"\n\ttoken drop probability = %.6g\n",((double)token_drop/(double)token_total));
	}
	if(((double)packets_accepted+(double)packets_dropped)==0){
		fprintf(stdout,"\tpacket drop probability = N/A, No packets have arrived into the system\n");
	}
	else{
		fprintf(stdout,"\tpacket drop probability = %.6g\n",((double)packets_dropped/((double)packets_accepted+(double)packets_dropped)));
	}
}

void* queue_thread(void *packet_arg){
	packet* prev_packet;
	packet* current_packet;
	double prev_packet_time=start_time;
	prev_packet=(packet *)packet_arg;
	int line_count=1;
	while(1){
		line_count++;
		current_packet=(packet*) malloc(sizeof(packet));
		prev_packet->id++;
		copy_packet(current_packet,prev_packet);
		if(prev_packet->id > number_of_packets){
			thread_done=1;
			break;
		}
		if(t_flag){
			if(fscanf(fp,"%lf %d %lf",&(current_packet->inter_arrival_time),&(current_packet->token_required),&(current_packet->service_time)) != 3){
				fprintf(stderr, "Error: input is not valid, at line %d\n",line_count);
				exit(1);
			}
		}
		else{
			current_packet->inter_arrival_time=(double)(1/current_packet->lambda)*1000;
			current_packet->service_time=(double)(1/current_packet->service_rate)*1000;
			if(current_packet->service_time>10000){
				current_packet->service_time=10000;
			}
			if(current_packet->inter_arrival_time>10000){
				current_packet->inter_arrival_time=10000;
			}

		}
		if(!current_packet->token_required){
			fprintf(stderr, "Error: Tokens required can't be negative or zero, at line %d\n",line_count);
		}
		double temp_time= current_packet->inter_arrival_time * 1000;
		usleep((int)temp_time);
		pthread_mutex_lock(&mutex);
		current_packet->inter_arrival_time_real=current_time()-prev_packet_time;
		packet_inter_arrival_time+=current_packet->inter_arrival_time_real;
		current_packet->q1_arrive=current_time()-start_time;
		packets_q1++;
		prev_packet_time=current_time();
		if(current_packet->token_required> token_bucket_height){
			fprintf(stdout,"%012.3lfms: p%d arrives, needs %d tokens,inter-arrival time = %.3lfms, dropped\n"
					,current_packet->q1_arrive,current_packet->id, current_packet->token_required, current_packet->inter_arrival_time_real);
			packets_dropped++;
			pthread_mutex_unlock(&mutex);
			continue;
		}
		packets_accepted++;
		if(current_packet->token_required==1){
			fprintf(stdout,"%012.3lfms: p%d arrives, needs %d token,inter-arrival time = %.3lfms\n"
					,current_packet->q1_arrive,current_packet->id, current_packet->token_required, current_packet->inter_arrival_time_real);
		}
		else{
			fprintf(stdout,"%012.3lfms: p%d arrives, needs %d tokens,inter-arrival time = %.3lfms\n"
					,current_packet->q1_arrive,current_packet->id, current_packet->token_required, current_packet->inter_arrival_time_real);	
		}
		current_packet->q1_start=current_time()-start_time;
		My402ListPrepend(&listq1,current_packet);
		fprintf(stdout, "%012.3lfms: p%d enters Q1\n",((packet*)listq1.anchor.next->obj)->q1_start,((packet*)listq1.anchor.next->obj)->id);
		if(My402ListLength(&listq1)==1){
			My402ListElem temp;
			int token_required=((packet*)listq1.anchor.prev->obj)->token_required;
			if(token_required <= token_count){
				token_count-=token_required;
				temp.obj=listq1.anchor.prev->obj;
				My402ListUnlink(&listq1,listq1.anchor.prev);
				((packet*)temp.obj)->q1_end=current_time()-start_time;
				fprintf(stdout, "%012.3lfms: p%d leaves Q1, time in Q1 = %.3lfms, token bucket now has %d token\n"
						,((packet*)temp.obj)->q1_end,((packet*)temp.obj)->id,((packet*)temp.obj)->q1_end-((packet*)temp.obj)->q1_start,token_count);
				time_in_q1+=((packet*)temp.obj)->q1_end - ((packet*)temp.obj)-> q1_start;
				My402ListPrepend(&listq2,((packet*)temp.obj));
				((packet*)listq2.anchor.next->obj)->q2_start = current_time()-start_time;
				fprintf(stdout, "%012.3lfms: p%d enters Q2\n"
						,((packet*)listq2.anchor.next->obj)->q2_start,((packet*)listq2.anchor.next->obj)->id);
				pthread_cond_broadcast(&cond_var);
			}
		}
		pthread_mutex_unlock(&mutex);
	}
	return 0;
}

void* token_thread(void *token_args){
	double prev_token_time=start_time;
	token *current_token;
	current_token=(token*)token_args;
	while(1){
		if(thread_done && My402ListEmpty(&listq1)){
			pthread_mutex_lock(&mutex);
			pthread_cond_broadcast(&cond_var);
			pthread_mutex_unlock(&mutex);
			break;
		}
		current_token->id ++;
		current_token->arrival_time=(double)(1/current_token->arrival_rate)*1000;
		double temp_time= (current_token->arrival_time) * 1000;
		usleep((int)temp_time);
		token_total++;
		pthread_mutex_lock(&mutex);
		token_count ++;
		current_token->arrival_time_real=current_time()-prev_token_time;
		prev_token_time=current_time();
		if(token_count > token_bucket_height){
			token_count--;
			token_drop++;
			fprintf(stdout, "%012.3lfms: token t%d arrives, dropped\n"
					, current_time()-start_time, current_token->id);
			pthread_mutex_unlock(&mutex);
		}
		else{
			if(token_count == 1){
				fprintf(stdout,"%012.3lfms: token t%d arrives, token bucket now has %d token\n"
						,current_time()-start_time, current_token->id,token_count);
			}
			else{
				fprintf(stdout,"%012.3lfms: token t%d arrives, token bucket now has %d tokens\n"
						,current_time()-start_time, current_token->id,token_count);
			}
			if(My402ListEmpty(&listq1)){
				pthread_mutex_unlock(&mutex);
			}
			else{
				if(((packet*)listq1.anchor.prev->obj)->token_required <= token_count){
					My402ListElem temp;
					int token_required=((packet*)listq1.anchor.prev->obj)->token_required;
					token_count-=token_required;
					temp.obj=listq1.anchor.prev->obj;
					My402ListUnlink(&listq1,listq1.anchor.prev);
					((packet*)temp.obj)->q1_end=current_time()-start_time;
					fprintf(stdout, "%012.3lfms: p%d leaves Q1, time in Q1 = %.3lfms, token bucket now has %d token\n"
							,((packet*)temp.obj)->q1_end,((packet*)temp.obj)->id,((packet*)temp.obj)->q1_end-((packet*)temp.obj)->q1_start,token_count);
					time_in_q1+=((packet*)temp.obj)->q1_end - ((packet*)temp.obj)-> q1_start;
					My402ListPrepend(&listq2,((packet*)temp.obj));
					((packet*)listq2.anchor.next->obj)->q2_start = current_time()-start_time;
					fprintf(stdout, "%012.3lfms: p%d enters Q2\n"
							,((packet*)listq2.anchor.next->obj)->q2_start,((packet*)listq2.anchor.next->obj)->id);
					if(!server.s1_ready || !server.s2_ready)
						pthread_cond_broadcast(&cond_var);
				}
				pthread_mutex_unlock(&mutex);
			}
		}
	}
	return 0;
}

void* server_thread(void* server_no_args){
	int server_no = *((int *) server_no_args);
	My402ListElem temp;
	while(1){
		pthread_mutex_lock(&mutex);
		while (My402ListEmpty(&listq2) && !cntrl_c_pressed && ((server.packets_s1+server.packets_s2) != (number_of_packets-packets_dropped))){
            pthread_cond_wait(&cond_var, &mutex); 
        }
        if(server.packets_s1+server.packets_s2 == number_of_packets-packets_dropped){
        	pthread_cancel(thread[signal_thread_no]);
			pthread_mutex_unlock(&mutex);
			break;
        }
        if(cntrl_c_pressed){
        	pthread_mutex_unlock(&mutex);
        	break;
        }
        temp.obj = listq2.anchor.prev->obj;
        My402ListUnlink(&listq2,listq2.anchor.prev);
        ((packet*)temp.obj)->q2_end=current_time()-start_time;
        fprintf(stdout, "%012.3lfms: p%d leaves Q2, time in Q2 = %.3lfms\n"
        		, ((packet*)temp.obj)->q2_end, ((packet*)temp.obj)->id, ((packet*)temp.obj)->q2_end-((packet*)temp.obj)->q2_start);
        time_in_q2+=((packet*)temp.obj)->q2_end-((packet*)temp.obj)->q2_start;
        if(server_no == 1){
        	server.s1_ready=1;
        	server.packets_s1++;
        }
        else if(server_no == 2){
        	server.s2_ready=1;
        	server.packets_s2++;
        }
        pthread_mutex_unlock(&mutex);
        if(server.s1_ready && server_no==1){
        	((packet*)temp.obj)->s1_start=current_time()-start_time;
        	pthread_mutex_lock(&mutex);
        	fprintf(stdout,"%012.3lfms: p%d begins service at S1, requesting %.3lfms of service\n"
        			,((packet*)temp.obj)->s1_start,((packet*)temp.obj)->id, ((packet*)temp.obj)->service_time);
        	pthread_mutex_unlock(&mutex);
        	double temp_time = ((packet*)temp.obj)->service_time * 1000; 
        	usleep((int)temp_time);
        	((packet*)temp.obj)->s1_end=current_time()-start_time;
        	pthread_mutex_lock(&mutex);
        	fprintf(stdout,"%012.3lfms: p%d departs from S1, service time = %.3lfms, time in system = %.3lfms\n"
        			, ((packet*)temp.obj)->s1_end,((packet*)temp.obj)->id,((packet*)temp.obj)->s1_end-((packet*)temp.obj)->s1_start,((packet*)temp.obj)->s1_end-((packet*)temp.obj)->q1_arrive);
        	total_system_time+=((packet*)temp.obj)->s1_end-((packet*)temp.obj)->q1_start;
        	server.time_s1+=((packet*)temp.obj)->s1_end-((packet*)temp.obj)->s1_start;
        	total_system_time_sq+=((((packet*)temp.obj)->s1_end-((packet*)temp.obj)->q1_arrive)*(((packet*)temp.obj)->s1_end-((packet*)temp.obj)->q1_arrive));
			pthread_mutex_unlock(&mutex);
			server.s1_ready=0;
		}
        if(server.s2_ready && server_no==2){
        	((packet*)temp.obj)->s2_start=current_time()-start_time;
        	pthread_mutex_lock(&mutex);
        	fprintf(stdout,"%012.3lfms: p%d begins service at S2, requesting %.3lfms of service\n"
        			,((packet*)temp.obj)->s2_start,((packet*)temp.obj)->id, ((packet*)temp.obj)->service_time);
        	pthread_mutex_unlock(&mutex);
        	double temp_time = ((packet*)temp.obj)->service_time * 1000; 
        	usleep((int)temp_time);
        	((packet*)temp.obj)->s2_end=current_time()-start_time;
        	pthread_mutex_lock(&mutex);
        	fprintf(stdout,"%012.3lfms: p%d departs from S2, service time = %.3lfms, time in system = %.3lfms\n"
        			, ((packet*)temp.obj)->s2_end,((packet*)temp.obj)->id,((packet*)temp.obj)->s2_end-((packet*)temp.obj)->s2_start,((packet*)temp.obj)->s2_end-((packet*)temp.obj)->q1_arrive);
        	total_system_time+=((packet*)temp.obj)->s2_end-((packet*)temp.obj)->q1_start;
        	server.time_s2+=((packet*)temp.obj)->s2_end-((packet*)temp.obj)->s2_start;
        	total_system_time_sq+=((((packet*)temp.obj)->s2_end-((packet*)temp.obj)->q1_arrive)*(((packet*)temp.obj)->s2_end-((packet*)temp.obj)->q1_arrive));
			pthread_mutex_unlock(&mutex);
			server.s2_ready=0;
        }

	}
	return 0;
}

void* signal_thread(){
	int sig=0;
	while(1){
		if(sigwait(&set,&sig)){
			fprintf(stderr, "Error: Signal Wait error\n");
			exit(1);
		}
		if(sig==SIGINT){
			pthread_mutex_lock(&mutex);
			cntrl_c_pressed=1;
			pthread_cond_broadcast(&cond_var);
			pthread_cancel(thread[token_thread_no]);
			pthread_cancel(thread[queue_thread_no]);
			pthread_mutex_unlock(&mutex);
			break;
		}
	}
	return 0; 
}

int intialize(packet *packet,token *token){
	server.packets_s1=0;
	server.packets_s2=0;
	server.s1_ready=0;
	server.s2_ready=0;
	My402ListUnlinkAll(&listq1);
	My402ListUnlinkAll(&listq2);
	sigemptyset(&set);
	sigaddset(&set,SIGINT);
	sigprocmask(SIG_BLOCK,&set, 0);
	t_flag=0;
	packet->lambda=1.0;
	packet->service_rate=0.35;
	token->arrival_rate=1.5;
	token_bucket_height=10;
	packet->token_required=3;
	number_of_packets=20;
	packet->id=0;
	return 0;
}

int main(int argc, char **argv){
	int count=1;
	int line_count=0;
	int *server_no1 = malloc(sizeof(*server_no1));
	int *server_no2 = malloc(sizeof(*server_no2));
	*server_no1 = 1;
	*server_no2 = 2;
	packet *packet_parm;
	token *token_parm;
	packet_parm=(packet*)malloc(sizeof(packet));
	token_parm=(token*) malloc(sizeof(token));
	if(intialize(packet_parm,token_parm) != 0){
		fprintf(stderr,"Couldn't intialize the system. Try again in sometime\n");
		exit(1);
	}
	//system("clear");
	if(argc%2 != 1 || argc < 1){
		fprintf(stderr, "Error:Malformed input, please follow the expected format,\n warmup2 [-lambda lambda] [-mu mu] [-r r] [-B B] [-P P] [-n num] [-t tsfile]\n");
		exit(1);	
	}
	while(count < argc){
		if(strcmp(argv[count],"-mu") == 0){
			if (sscanf(argv[count+1], "%lf", &(packet_parm->service_rate)) != 1) {
					fprintf(stderr, "Error: input flag \"-mu\" is not valid\n");
					exit(1);
				}
		}
		else if(strcmp(argv[count],"-lambda") == 0){
			if (sscanf(argv[count+1], "%lf", &(packet_parm->lambda)) != 1) {
					fprintf(stderr, "Error: input flag \"-lambda\" is not valid\n");
					exit(1);
				}
		}
		else if(strcmp(argv[count],"-r") == 0){
			if (sscanf(argv[count+1], "%lf", &(token_parm->arrival_rate)) != 1) {
					fprintf(stderr, "Error: input flag \"-r\" is not valid\n");
					exit(1);
				}
		}
		else if(strcmp(argv[count],"-B") == 0){
			if (sscanf(argv[count+1], "%d", &token_bucket_height) != 1) {
					fprintf(stderr, "Error: input flag \"-B\" is not valid\n");
					exit(1);
				}
		}
		else if(strcmp(argv[count],"-P") == 0){
			if (sscanf(argv[count+1], "%d", &(packet_parm->token_required)) != 1) {
					fprintf(stderr, "Error: input flag \"-P\" is not valid\n");
					exit(1);
				}			
		}
		else if(strcmp(argv[count],"-n") == 0){
			if (sscanf(argv[count+1], "%d", &number_of_packets) != 1) {
					fprintf(stderr, "Error: input flag \"-n\" is not valid\n");
					exit(1);
				}
		}
		else if(strcmp(argv[count],"-t") == 0){
			if(strlen(argv[count+1]) == 0){
				fprintf(stderr, "Error: input flag \"-t\" can't be empty\n");
			}
			strcpy(tsfile,argv[count+1]);
			t_flag=1;
			fp=fopen(tsfile,"r");
			if(fp == NULL){
				fprintf(stderr, "Error: opening the file \"%s\" not successful, %s ",tsfile, strerror(errno));
				exit(1);
			}
			struct stat statbuf;
			stat(tsfile, &statbuf);
			if(S_ISDIR(statbuf.st_mode)){
				fprintf(stderr, "Error: The specified path \"%s\" is a directory\n",tsfile);
				exit(1);
			}
		}
		else{
			fprintf(stderr, "Error:Malformed input, please follow the expected format,\nwarmup2 [-lambda lambda] [-mu mu] [-r r] [-B B] [-P P] [-n num] [-t tsfile]\n");
			exit(1);	
		}
		count +=2 ;
	}	
	if(t_flag){
		line_count++;
		if(fscanf(fp,"%d",&number_of_packets) != 1){
			fprintf(stderr,"Error: tfile has a non int value in line %d\n",line_count);
			exit(1);
		}
	}
	display_parameters(packet_parm,token_parm);
	if(!My402ListInit(&listq1)){
		fprintf(stderr,"Error: Can't intialize the queue Q1\n");
		exit(1);
	}
	if(!My402ListInit(&listq2)){
		fprintf(stderr,"Error: Can't intialize the queue Q2\n");
		exit(1);
	}
	start_time = current_time();
	fprintf(stdout,"%012.3lfms: emulation begins\n",start_time-start_time);
	if(pthread_create(&thread[queue_thread_no],0,queue_thread,packet_parm)){
		fprintf(stderr, " Error: Queue thread can't be created");
		exit(1);
	}
	if(pthread_create(&thread[token_thread_no],0,token_thread,token_parm)){
		fprintf(stderr, "Error: Token thread can't be created");
		exit(1);
	}
	if(pthread_create(&thread[server1_thread_no],0,server_thread,server_no1)){
		fprintf(stderr, "Error: Server1 thread can't be created");
		exit(1);
	}
	if(pthread_create(&thread[server2_thread_no],0,server_thread,server_no2)){
		fprintf(stderr, "Error: Server2 thread can't be created");
		exit(1);
	}
	if(pthread_create(&thread[signal_thread_no],0,signal_thread,NULL)){
		fprintf(stderr, "Error: Signal thread can't be created");
		exit(1);
	}
	if(pthread_join(thread[queue_thread_no],NULL)){
		fprintf(stderr,"Error: Can't join to Queue thread\n");
		exit(1);
	}
	if(pthread_join(thread[token_thread_no],NULL)){
		fprintf(stderr,"Error: Can't join to Token thread\n");
		exit(1);
	}
	if(pthread_join(thread[server1_thread_no],NULL)){
		fprintf(stderr,"Error: Can't join to Server1 thread\n");
		exit(1);
	}
	if(pthread_join(thread[server2_thread_no],NULL)){
		fprintf(stderr,"Error: Can't join to Server2 thread\n");
		exit(1);
	}
	if(pthread_join(thread[signal_thread_no],NULL)){
		fprintf(stderr,"Error: Can't join to Signal thread\n");
		exit(1);
	}
	if(t_flag){
		fclose(fp);
	}
	display_statistics();
	return 0;
}