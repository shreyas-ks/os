Documentation for Warmup Assignment 1
=====================================

+-------+
| BUILD |
+-------+

Comments: just use the make command to create executale file, make clean to clean to binary files 

+-----------------+
| SKIP (Optional) |
+-----------------+

+---------+
| GRADING |
+---------+

(A) Doubly-linked Circular List : 40 out of 40 pts

(B.1) Sort (file) : 30 out of 30 pts
(B.2) Sort (stdin) :30 out of 30 pts

Missing required section(s) in README file : 0
Cannot compile : 0
Compiler warnings : 0
"make clean" : 0
Segmentation faults : 0
Separate compilation : 0
Malformed input : 0
Too slow : None of the case
Bad commandline : if the the excetuable is run without the sort as the parameter
Did not use My402List and My402ListElem to implement "sort" in (B) : No have used the same.

+------+
| BUGS |
+------+
No bugs with the tests provided and other malformed inputs
Comments: Runs within the given specs

+------------------+
| OTHER (Optional) |
+------------------+

Comments on design decisions: Have used cirluar doubly linked list with sorting based on time stamp, that is later parsed into 
displayable format with ctime function 
Comments on deviation from spec: None

