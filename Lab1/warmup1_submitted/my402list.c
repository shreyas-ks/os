#include "my402list.h"
#include <stdio.h>
#include <stdlib.h>

int My402ListLength(My402List* list){
	return list->num_members;
}
int My402ListEmpty(My402List* list){
	return (My402ListLength(list) == 0) ? TRUE:FALSE;
}
int My402ListAppend(My402List* list, void* object){
	My402ListElem *ele;
	ele =(My402ListElem *)malloc(sizeof(My402ListElem));
	if(ele == '\0'){
		return FALSE;
	}
	ele -> obj = object;
	if(My402ListLength(list)== 0){
		ele -> next = &(list->anchor);
		list->anchor.prev = ele;
		list->anchor.next = ele;
		ele -> prev = &(list->anchor);
		list->num_members++;
	}
	else{
			My402ListElem *temp=My402ListLast(list);
			temp->next=ele;
			ele->next=&(list->anchor);
			ele->prev=temp;
			list->anchor.prev=ele;
			list->num_members++;
		}
	return TRUE;
}
int My402ListPrepend(My402List* list, void* object){
	My402ListElem *ele;
	ele =(My402ListElem *)malloc(sizeof(My402ListElem));
	if(ele == '\0'){
		return FALSE;
	}
	ele -> obj = object;
	if(My402ListLength(list) == 0){
		ele->prev= &(list->anchor);
		list->anchor.next = ele;
		ele->next = &(list->anchor);
		list->anchor.prev = ele;
		list->num_members++;
	}
	else{
			My402ListElem *temp=My402ListFirst(list);
			temp->prev=ele;
			ele->next=temp;
			ele->prev=&(list->anchor);
			list->anchor.next=ele;
			list->num_members++;
		}
	return TRUE;
}
void My402ListUnlink(My402List* list, My402ListElem* ele){
	ele->next->prev = ele->prev;
	ele->prev->next = ele->next;
	if(list->num_members >0)
		list->num_members--;
	free(ele);
}
void My402ListUnlinkAll(My402List* list){
	int i=0;
	My402ListElem *temp=list->anchor.next;
	My402ListElem *temp2;
	for(i=0;i<list->num_members;i++){
		temp2 = temp->next;
		free(temp);
		temp = temp2;
		if(list->num_members >0)
			list->num_members--;
	}
}
int  My402ListInsertAfter(My402List* list, void* object, My402ListElem* ele){
	My402ListElem *insele;
	if(ele == '\0'){
		My402ListAppend(list,object);
	}
	else{
		insele =(My402ListElem *)malloc(sizeof(My402ListElem));
		if(insele == '\0'){
			return FALSE;
		}
		insele -> obj = object;
		insele->next=ele->next;
		insele->prev=ele;
		insele->next->prev=insele;
		insele->prev->next=insele;
		list->num_members++;
	}
	return TRUE;
}
int  My402ListInsertBefore(My402List* list, void* object, My402ListElem* ele){
	My402ListElem *insele;
	if(ele == '\0'){
		My402ListPrepend(list,object);
	}
	else{
		insele =(My402ListElem *)malloc(sizeof(My402ListElem));
		if(insele == '\0'){
			return FALSE;
		}
		insele->obj = object;
		insele->prev=ele->prev;
		insele->next=ele;
		insele->prev->next=insele;
		insele->next->prev=insele;
		list->num_members++;
	}
	return TRUE;
}
My402ListElem* My402ListFirst(My402List *list){
	if(!My402ListEmpty(list))
		return list->anchor.next;
	return '\0';
}	
My402ListElem* My402ListLast(My402List *list){
	if(!My402ListEmpty(list))
		return list->anchor.prev;
	return '\0';
}
My402ListElem *My402ListNext(My402List* list, My402ListElem* ele){
	if(My402ListLast(list) != ele)
		return ele->next;
	return '\0';
}
My402ListElem *My402ListPrev(My402List* list, My402ListElem* ele){
	if(My402ListFirst(list) != ele)
		return ele->prev;
	return '\0';
}
My402ListElem *My402ListFind(My402List* list, void* object){
	My402ListElem *ele = NULL;
	for(ele=My402ListFirst(list);ele!=NULL;ele=My402ListNext(list,ele)){
		if(ele->obj == object){
			return ele;
		}
	}
	return '\0';
}

int My402ListInit(My402List* list){
	if(list == '\0'){
		return FALSE;
	}
	list->num_members=0;
	list->anchor.next = &(list->anchor);
	list->anchor.prev = &(list->anchor);
	list->anchor.obj='\0';
	return TRUE;
}

