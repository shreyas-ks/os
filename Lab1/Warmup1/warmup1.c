/******************************************************************************/
/* Important CSCI 402 usage information:                                      */
/*                                                                            */
/* This fils is part of CSCI 402 programming assignments at USC.              */
/*         53616c7465645f5f2e8d450c0c5851acd538befe33744efca0f1c4f9fb5f       */
/*         3c8feabc561a99e53d4d21951738da923cd1c7bbd11b30a1afb11172f80b       */
/*         984b1acfbbf8fae6ea57e0583d2610a618379293cb1de8e1e9d07e6287e8       */
/*         de7e82f3d48866aa2009b599e92c852f7dbf7a6e573f1c7228ca34b9f368       */
/*         faaef0c0fcf294cb                                                   */
/* Please understand that you are NOT permitted to distribute or publically   */
/*         display a copy of this file (or ANY PART of it) for any reason.    */
/* If anyone (including your prospective employer) asks you to post the code, */
/*         you must inform them that you do NOT have permissions to do so.    */
/* You are also NOT permitted to remove or alter this comment block.          */
/* If this comment block is removed or altered in a submitted file, 20 points */
/*         will be deducted.                                                  */
/******************************************************************************/

/*
 * Author:      William Chia-Wei Cheng (bill.cheng@acm.org)
 *
 * @(#)$Id: listtest.c,v 1.1 2016/08/18 05:24:29 william Exp $
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>

#include "cs402.h"

#include "my402list.h"

static char gszProgName[MAXPATHLENGTH];

struct object{
    char type;
    unsigned int timestamp;
    double ammount;
    char summary[1024];
};
typedef struct object lineobject;
/* ----------------------- Utility Functions ----------------------- */

static
void SetProgramName(char *s)
{
    char *c_ptr=strrchr(s, DIR_SEP);

    if (c_ptr == NULL) {
        strcpy(gszProgName, s);
    } else {
        strcpy(gszProgName, ++c_ptr);
    }
}

/* ----------------------- Test() ----------------------- */

static
void FindInList(My402List *pList,unsigned int time)
{
    int i=0;
    My402ListElem *elem=NULL;
    if(!My402ListEmpty(pList)){
        for (elem=My402ListFirst(pList); elem != NULL; elem=My402ListNext(pList, elem)) {
            i++;
            lineobject *object=(elem->obj);
            unsigned int timestamp_value=object->timestamp;
            if(timestamp_value == time ){
                if(time == 0)
                    fprintf(stderr,"Error: Line size is more than 1024 characters line:%d\n",i-1);
                else
                    fprintf(stderr, "Error: The time stamp \"%d\" is repeated in line %d \n",time,i);
                exit(1);
            }
        }
    }
}

void string_reverse(char * string){
    char *end = string+strlen(string)-1;
    char temp;
    while(string < end){
        temp=*string;
        *string=*end;
        *end=temp;
        string++;
        end--;
    }
}

void floatToString(double number,char* string){
    int temp=number*1000;
    int count=0;
    int decimal_count=0;
    int comma_count=-2; 
    int temp2=0;
    temp2 = (temp % 10);
    temp = temp / 10;
    if(temp2 == 9)
        temp+=1;
    int integer_part=(int)temp;
    while(integer_part || count<4){
        if(decimal_count == 2){
            string[count] = '.';
            count++;
        }
        if(comma_count == 3){
            string[count] = ',';
            comma_count=0;
            count++;
        }
        if(number > 10000000)
            string[count]='?';
        else
            string[count] = 48+(integer_part%10);
        integer_part = integer_part/10;
        count++;
        decimal_count++;
        comma_count++;
    }
    string[count] = '\0';
    if(*(string+strlen(string)-1) == '?')
        *(string+12) = '\0';
    string_reverse(string);
}

static
void PrintList(My402List *pList)
{
    My402ListElem *elem=NULL;
    char ammount_string[20];
    char balance_string[20];
    double balance=0;
    double ab_balance=0;
    printf("+-----------------+--------------------------+----------------+----------------+\n");
    printf("|       Date      | Description              |         Amount |        Balance |\n");
    printf("+-----------------+--------------------------+----------------+----------------+\n");
    for (elem=My402ListFirst(pList); elem != NULL; elem=My402ListNext(pList, elem)) {
        lineobject *object=(elem->obj);
        time_t timestamp_value=(time_t)object->timestamp;
        char *time_string=ctime(&timestamp_value);
        time_string[strcspn(time_string, "\n")] = 0;
        floatToString(object->ammount,ammount_string);
        if(object->type == '+'){
            balance+=object->ammount;
            if(balance < 0){
                ab_balance=balance-2*balance;
                floatToString(ab_balance,balance_string);
                fprintf(stdout,"| %10.10s%5.5s | %-24.24s |  %12s  | (%12s) |\n"
                ,time_string,strcat(time_string + strlen(time_string) - 5,"\0"),object->summary,ammount_string,balance_string);
            }
            else{
                ab_balance=balance;
                floatToString(ab_balance,balance_string);
                fprintf(stdout,"| %10.10s%5.5s | %-24.24s |  %12s  |  %12s  |\n"
                ,time_string,strcat(time_string + strlen(time_string) - 5,"\0"),object->summary,ammount_string,balance_string);
            }
        }
        if(object->type == '-'){
            balance-=object->ammount;
            if(balance < 0){
                ab_balance=balance-2*balance;
                floatToString(ab_balance,balance_string);
                fprintf(stdout,"| %10.10s%5.5s | %-24.24s | (%12s) | (%12s) |\n"
                ,time_string,strcat(time_string + strlen(time_string) - 5,"\0"),object->summary,ammount_string,balance_string);
            }
            else{
                ab_balance=balance;
                floatToString(ab_balance,balance_string);
                fprintf(stdout,"| %10.10s%5.5s | %-24.24s | (%12s) |  %12s  |\n"
                ,time_string,strcat(time_string + strlen(time_string) - 5,"\0"),object->summary,ammount_string,balance_string);
            }
        }
    }   
    printf("+-----------------+--------------------------+----------------+----------------+\n");
}

static
void BubbleForward(My402List *pList, My402ListElem **pp_elem1, My402ListElem **pp_elem2)
{
    My402ListElem *elem1=(*pp_elem1), *elem2=(*pp_elem2);
    void *obj1=elem1->obj, *obj2=elem2->obj;
    My402ListElem *elem1prev=My402ListPrev(pList, elem1);
    My402ListElem *elem2next=My402ListNext(pList, elem2);

    My402ListUnlink(pList, elem1);
    My402ListUnlink(pList, elem2);
    if (elem1prev == NULL) {
        (void)My402ListPrepend(pList, obj2);
        *pp_elem1 = My402ListFirst(pList);
    } else {
        (void)My402ListInsertAfter(pList, obj2, elem1prev);
        *pp_elem1 = My402ListNext(pList, elem1prev);
    }
    if (elem2next == NULL) {
        (void)My402ListAppend(pList, obj1);
        *pp_elem2 = My402ListLast(pList);
    } else {
        (void)My402ListInsertBefore(pList, obj1, elem2next);
        *pp_elem2 = My402ListPrev(pList, elem2next);
    }
}

static
void BubbleSortForwardList(My402List *pList)
{
    My402ListElem *elem=NULL;
    int i=0;

    for (i=0; i < pList->num_members; i++) {
        int j=0, something_swapped=FALSE;
        My402ListElem *next_elem=NULL;

        for (elem=My402ListFirst(pList), j=0; j < pList->num_members-i-1; elem=next_elem, j++) {
            lineobject *current_object=elem->obj;
            lineobject *next_object=NULL;
            unsigned int cur_val=(unsigned int)current_object->timestamp, next_val=0;

            next_elem=My402ListNext(pList, elem);
            next_object = next_elem->obj;
            next_val=(unsigned int)next_object->timestamp;

            if (cur_val > next_val) {
                BubbleForward(pList, &elem, &next_elem);
                something_swapped = TRUE;
            }
        }
        if (!something_swapped) break;
    }
}

static
void ParseElements(char *filename)
{
    My402List list;
    
    FILE *fp=NULL;
    FILE *fp2=NULL;
    char type=NULL;
    unsigned int timestamp=0;
    double ammount;
    char summary[1024];
    int count_lines=0;
    int transactionFlag=0;
    
    lineobject *obj;
    
    memset(&list, 0, sizeof(My402List));
    (void)My402ListInit(&list);
    if(filename == '\0'){
        fp=stdin;
        fp2=stdin;
    }
    else{
        fp=fopen(filename,"r");
        if(fp == '\0'){
            fprintf(stderr,"Error: No such file exists\n");
            exit(1);
        }
        fp2=fopen(filename,"r");
    }
    while(!feof(fp)){ 
        int i=0,count=0;
        count_lines++;
        obj=(lineobject*)malloc(sizeof(lineobject));
        fscanf(fp,"%c\t %d\t %lf",&type,&timestamp,&ammount);
        if(timestamp > 9999999999){
            fprintf(stderr,"Error: The timestamp \"%d\" is invalid\n",timestamp);
            exit(1);
        }
        if(ammount < 0){
                fprintf(stderr,"Error: Transaction ammount can't be negative, Line:\"%d\"\n",count_lines);
            exit(1);
        }
        FindInList(&list,timestamp);
        if(ammount > 10000000){
            fprintf(stderr,"Error: The transaction ammount is greater that 10 million!\n");
            exit(1);
        }
        fgets(summary,sizeof(summary),fp);
        summary[strcspn(summary, "\n")] = 0;
        if(strlen(summary) != 0 && (type== '+' || type=='-'))
            transactionFlag=1;
        obj->type = type;
        obj->timestamp = timestamp;
        obj->ammount = ammount;
        while(summary[i]!='\0'){
            if(summary[i]==' ' || summary[i]== '\t'){
                count++;
                i++;
            }
            else
                break;
        }   
        strcpy(obj->summary,summary+count);
        if(strlen(summary)-count <= 0 && count!=0){
            fprintf(stderr,"Error: Description Empty\n");
            exit(1);
        }
        My402ListAppend(&list,obj);
        type=0;
        ammount=0;
        summary[0]='\0';
        timestamp=0;
    }
    if(transactionFlag != 1){
        fprintf(stderr,"Error: No proper Transaction in the file\n");
        exit(1);
    }
    BubbleSortForwardList(&list);
    PrintList(&list);
    My402ListUnlinkAll(&list);
}

/* ----------------------- main() ----------------------- */

int main(int argc, char *argv[])
{
    SetProgramName(*argv); 
    if(argc < 2){
        fprintf(stderr,"Error: less number of arguments for the Command\n"); 
        exit(1);
    }
    if(!strcmp("sort",argv[1])) 
        ParseElements(argv[2]);
    else
       fprintf(stderr,"Error: Command format improper\n"); 
    return(0);
}
