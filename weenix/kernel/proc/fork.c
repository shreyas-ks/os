/******************************************************************************/
/* Important Fall 2016 CSCI 402 usage information:                            */
/*                                                                            */
/* This fils is part of CSCI 402 kernel programming assignments at USC.       */
/*         53616c7465645f5f2e8d450c0c5851acd538befe33744efca0f1c4f9fb5f       */
/*         3c8feabc561a99e53d4d21951738da923cd1c7bbd11b30a1afb11172f80b       */
/*         984b1acfbbf8fae6ea57e0583d2610a618379293cb1de8e1e9d07e6287e8       */
/*         de7e82f3d48866aa2009b599e92c852f7dbf7a6e573f1c7228ca34b9f368       */
/*         faaef0c0fcf294cb                                                   */
/* Please understand that you are NOT permitted to distribute or publically   */
/*         display a copy of this file (or ANY PART of it) for any reason.    */
/* If anyone (including your prospective employer) asks you to post the code, */
/*         you must inform them that you do NOT have permissions to do so.    */
/* You are also NOT permitted to remove or alter this comment block.          */
/* If this comment block is removed or altered in a submitted file, 20 points */
/*         will be deducted.                                                  */
/******************************************************************************/

#include "types.h"
#include "globals.h"
#include "errno.h"

#include "util/debug.h"
#include "util/string.h"

#include "proc/proc.h"
#include "proc/kthread.h"

#include "mm/mm.h"
#include "mm/mman.h"
#include "mm/page.h"
#include "mm/pframe.h"
#include "mm/mmobj.h"
#include "mm/pagetable.h"
#include "mm/tlb.h"

#include "fs/file.h"
#include "fs/vnode.h"

#include "vm/shadow.h"
#include "vm/vmmap.h"

#include "api/exec.h"

#include "main/interrupt.h"

/* Pushes the appropriate things onto the kernel stack of a newly forked thread
 * so that it can begin execution in userland_entry.
 * regs: registers the new thread should have on execution
 * kstack: location of the new thread's kernel stack
 * Returns the new stack pointer on success. */
static uint32_t
fork_setup_stack(const regs_t *regs, void *kstack)
{
        /* Pointer argument and dummy return address, and userland dummy return
         * address */
        uint32_t esp = ((uint32_t) kstack) + DEFAULT_STACK_SIZE - (sizeof(regs_t) + 12);
        *(void **)(esp + 4) = (void *)(esp + 8); /* Set the argument to point to location of struct on stack */
        memcpy((void *)(esp + 8), regs, sizeof(regs_t)); /* Copy over struct */
        return esp;
}


/*
 * The implementation of fork(2). Once this works,
 * you're practically home free. This is what the
 * entirety of Weenix has been leading up to.
 * Go forth and conquer.
 */
int
do_fork(struct regs *regs)
{
		KASSERT(regs != NULL);
		dbg(DBG_PRINT, "(GRADING3A 7.a)\n");
	    KASSERT(curproc != NULL);
	    dbg(DBG_PRINT, "(GRADING3A 7.a)\n");
	    KASSERT(curproc->p_state == PROC_RUNNING);
	    dbg(DBG_PRINT, "(GRADING3A 7.a)\n");

	    proc_t *process = proc_create("childproc");
		process->p_vmmap = vmmap_clone(curproc->p_vmmap);
		KASSERT(process->p_state == PROC_RUNNING);
	    dbg(DBG_PRINT, "(GRADING3A 7.a)\n");

	    vmarea_t *parent_vma;
		list_iterate_begin(&curproc->p_vmmap->vmm_list, parent_vma, vmarea_t, vma_plink) {
		if(parent_vma->vma_flags & MAP_PRIVATE){
				mmobj_t *shadow = shadow_create();
				shadow->mmo_shadowed = parent_vma->vma_obj;
				shadow->mmo_un.mmo_bottom_obj = mmobj_bottom_obj(parent_vma->vma_obj);
				parent_vma->vma_obj = shadow;
				parent_vma->vma_obj->mmo_un.mmo_bottom_obj = shadow->mmo_un.mmo_bottom_obj;
			}
		}
		list_iterate_end();

		vmarea_t *child_vma;
		list_iterate_begin(&process->p_vmmap->vmm_list, child_vma, vmarea_t, vma_plink) {
			if(child_vma->vma_flags & MAP_PRIVATE){
				mmobj_t *shadow = shadow_create();
				shadow->mmo_shadowed = child_vma->vma_obj;
				shadow->mmo_un.mmo_bottom_obj = child_vma->vma_obj->mmo_un.mmo_bottom_obj;
				child_vma->vma_obj = shadow;
				child_vma->vma_obj->mmo_un.mmo_bottom_obj = shadow->mmo_un.mmo_bottom_obj;
			}
		}
		list_iterate_end();

		pt_unmap_range(curproc->p_pagedir, USER_MEM_LOW, USER_MEM_HIGH);
		tlb_flush_all();
		process->p_brk = curproc->p_brk;
		process->p_cwd = curproc->p_cwd;
		process->p_start_brk = curproc->p_start_brk;
		int i;
		for (i=0; i<NFILES;i++)
		{
			process->p_files[i]= curproc->p_files[i];
			if(process->p_files[i] == NULL) continue;
			fref(process->p_files[i]);
		}
		kthread_t *newthr = kthread_clone(curthr);
		newthr->kt_proc = process;
		list_insert_tail(&process->p_threads, &newthr->kt_plink);
		regs->r_eax = 0;
		newthr->kt_ctx.c_kstack = (uintptr_t) newthr->kt_kstack;
		newthr->kt_ctx.c_kstacksz = DEFAULT_STACK_SIZE;
		newthr->kt_ctx.c_esp = fork_setup_stack(regs, newthr->kt_kstack);
		newthr->kt_ctx.c_pdptr = process->p_pagedir;
		newthr->kt_ctx.c_eip = (uint32_t) userland_entry;
		KASSERT(process->p_pagedir != NULL);
		dbg(DBG_PRINT, "(GRADING3A 7.a)\n");
		KASSERT(newthr->kt_kstack != NULL);
		dbg(DBG_PRINT, "(GRADING3A 7.a)\n");
		sched_make_runnable(newthr);
		regs->r_eax = process->p_pid;
		dbg(DBG_PRINT,"CODEPATH(1): Inside do_fork,made child thread runnable\n");
		return process->p_pid;
}
