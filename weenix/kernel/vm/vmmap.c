/******************************************************************************/
/* Important Fall 2016 CSCI 402 usage information:                            */
/*                                                                            */
/* This fils is part of CSCI 402 kernel programming assignments at USC.       */
/*         53616c7465645f5f2e8d450c0c5851acd538befe33744efca0f1c4f9fb5f       */
/*         3c8feabc561a99e53d4d21951738da923cd1c7bbd11b30a1afb11172f80b       */
/*         984b1acfbbf8fae6ea57e0583d2610a618379293cb1de8e1e9d07e6287e8       */
/*         de7e82f3d48866aa2009b599e92c852f7dbf7a6e573f1c7228ca34b9f368       */
/*         faaef0c0fcf294cb                                                   */
/* Please understand that you are NOT permitted to distribute or publically   */
/*         display a copy of this file (or ANY PART of it) for any reason.    */
/* If anyone (including your prospective employer) asks you to post the code, */
/*         you must inform them that you do NOT have permissions to do so.    */
/* You are also NOT permitted to remove or alter this comment block.          */
/* If this comment block is removed or altered in a submitted file, 20 points */
/*         will be deducted.                                                  */
/******************************************************************************/

#include "kernel.h"
#include "errno.h"
#include "globals.h"

#include "vm/vmmap.h"
#include "vm/shadow.h"
#include "vm/anon.h"

#include "proc/proc.h"

#include "util/debug.h"
#include "util/list.h"
#include "util/string.h"
#include "util/printf.h"

#include "fs/vnode.h"
#include "fs/file.h"
#include "fs/fcntl.h"
#include "fs/vfs_syscall.h"

#include "mm/slab.h"
#include "mm/page.h"
#include "mm/mm.h"
#include "mm/mman.h"
#include "mm/mmobj.h"

static slab_allocator_t *vmmap_allocator;
static slab_allocator_t *vmarea_allocator;

void
vmmap_init(void)
{
	vmmap_allocator = slab_allocator_create("vmmap", sizeof(vmmap_t));
	KASSERT(NULL != vmmap_allocator && "failed to create vmmap allocator!");
	vmarea_allocator = slab_allocator_create("vmarea", sizeof(vmarea_t));
	KASSERT(NULL != vmarea_allocator && "failed to create vmarea allocator!");
}

vmarea_t *
vmarea_alloc(void)
{
	vmarea_t *newvma = (vmarea_t *) slab_obj_alloc(vmarea_allocator);
	if (newvma) {
		newvma->vma_vmmap = NULL;
	}
	return newvma;
}

void
vmarea_free(vmarea_t *vma)
{
	KASSERT(NULL != vma);
	slab_obj_free(vmarea_allocator, vma);
}

/* a debugging routine: dumps the mappings of the given address space. */
size_t
vmmap_mapping_info(const void *vmmap, char *buf, size_t osize)
{
	KASSERT(0 < osize);
	KASSERT(NULL != buf);
	KASSERT(NULL != vmmap);

	vmmap_t *map = (vmmap_t *)vmmap;
	vmarea_t *vma;
	ssize_t size = (ssize_t)osize;

	int len = snprintf(buf, size, "%21s %5s %7s %8s %10s %12s\n",
			"VADDR RANGE", "PROT", "FLAGS", "MMOBJ", "OFFSET",
			"VFN RANGE");

	list_iterate_begin(&map->vmm_list, vma, vmarea_t, vma_plink) {
		size -= len;
		buf += len;
		if (0 >= size) {
			goto end;
		}

		len = snprintf(buf, size,
				"%#.8x-%#.8x  %c%c%c  %7s 0x%p %#.5x %#.5x-%#.5x\n",
				vma->vma_start << PAGE_SHIFT,
				vma->vma_end << PAGE_SHIFT,
				(vma->vma_prot & PROT_READ ? 'r' : '-'),
				(vma->vma_prot & PROT_WRITE ? 'w' : '-'),
				(vma->vma_prot & PROT_EXEC ? 'x' : '-'),
				(vma->vma_flags & MAP_SHARED ? " SHARED" : "PRIVATE"),
				vma->vma_obj, vma->vma_off, vma->vma_start, vma->vma_end);
	} list_iterate_end();

	end:
	if (size <= 0) {
		size = osize;
		buf[osize - 1] = '\0';
	}
	/*
        KASSERT(0 <= size);
        if (0 == size) {
                size++;
                buf--;
                buf[0] = '\0';
        }
	 */
	return osize - size;
}

/* Create a new vmmap, which has no vmareas and does
 * not refer to a process. */
vmmap_t *
vmmap_create(void)
{
	dbg(DBG_PRINT,"CODEPATH : vmmap_create\n");
	vmmap_t *vmmap=slab_obj_alloc(vmmap_allocator);
	list_init(&vmmap->vmm_list);
	vmmap->vmm_proc = NULL;
	return vmmap;
}

/* Removes all vmareas from the address space and frees the
 * vmmap struct. */
void
vmmap_destroy(vmmap_t *map)
{
	vmarea_t *temp;
	dbg(DBG_PRINT,"CODEPATH : vmmap_destroy\n");
	KASSERT(NULL != map);
    dbg(DBG_PRINT, "(GRADING3A 3.a)\n");
	list_iterate_begin(&map->vmm_list, temp, vmarea_t, vma_plink) {
		if(temp->vma_obj != NULL){
			temp->vma_obj->mmo_ops->put(temp->vma_obj);
		}
		list_remove(&temp->vma_plink);
		if (list_link_is_linked(&temp->vma_olink)){
			list_remove(&temp->vma_olink);
		}
		vmarea_free(temp);
	} list_iterate_end();
	slab_obj_free(vmmap_allocator, map);
}

/* Add a vmarea to an address space. Assumes (i.e. asserts to some extent)
 * the vmarea is valid.  This involves finding where to put it in the list
 * of VM areas, and adding it. Don't forget to set the vma_vmmap for the
 * area. */
void
vmmap_insert(vmmap_t *map, vmarea_t *newvma)
{
	vmarea_t *temp;
	KASSERT(NULL != map && NULL != newvma);
    dbg(DBG_PRINT, "(GRADING3A 3.b)\n");
	KASSERT(NULL == newvma->vma_vmmap);
    dbg(DBG_PRINT, "(GRADING3A 3.b)\n");
	KASSERT(newvma->vma_start < newvma->vma_end);
    dbg(DBG_PRINT, "(GRADING3A 3.b)\n");
	KASSERT(ADDR_TO_PN(USER_MEM_LOW) <= newvma->vma_start && ADDR_TO_PN(USER_MEM_HIGH) >= newvma->vma_end);
	dbg(DBG_PRINT, "(GRADING3A 3.b)\n");

	newvma->vma_vmmap = map;
	list_iterate_begin(&map->vmm_list, temp, vmarea_t, vma_plink) {
	if((temp->vma_start > newvma->vma_start)){
		list_insert_before(&temp->vma_plink, &newvma->vma_plink);
		return;
	}
	}
	list_iterate_end();
	list_insert_tail(&map->vmm_list,&newvma->vma_plink);
}

/* Find a contiguous range of free virtual pages of length npages in
 * the given address space. Returns starting vfn for the range,
 * without altering the map. Returns -1 if no such range exists.
 *
 * Your algorithm should be first fit. If dir is VMMAP_DIR_HILO, you
 * should find a gap as high in the address space as possible; if dir
 * is VMMAP_DIR_LOHI, the gap should be as low as possible. */
int
vmmap_find_range(vmmap_t *map, uint32_t npages, int dir)
{

	uint32_t lmemory,hmemory;
	lmemory = USER_MEM_LOW / PAGE_SIZE;
	hmemory = USER_MEM_HIGH / PAGE_SIZE;

	vmarea_t *temp;
	if(dir == VMMAP_DIR_HILO)
	{
		list_iterate_reverse(&map->vmm_list, temp, vmarea_t, vma_plink){
				if (temp->vma_plink.l_next == &map->vmm_list){
				if (hmemory - temp->vma_end >= npages)
						return (hmemory - npages);
				}
				else{
					vmarea_t *next = list_item(temp->vma_plink.l_next,vmarea_t,vma_plink);
					if((next->vma_start - temp->vma_end) >= npages)
							return (next->vma_start - npages);
					}
				}
				list_iterate_end();
				if (temp->vma_start - lmemory >= npages) return (temp->vma_start - npages);
	}
	else if(dir == VMMAP_DIR_LOHI)
	{
		list_iterate_begin(&map->vmm_list, temp, vmarea_t, vma_plink) {
				if (temp->vma_plink.l_prev != &map->vmm_list){
					vmarea_t *next = list_item(temp->vma_plink.l_next,vmarea_t,vma_plink);
					if((next->vma_start - temp->vma_end) >= npages) return temp->vma_end;
					}
					else{
						if (temp->vma_start - lmemory >= npages) return temp->vma_start - npages;
					}
				}
				list_iterate_end();
				if (hmemory - temp->vma_end >= npages) return (temp->vma_end);

	}
	return -1;
}

/* Find the vm_area that vfn lies in. Simply scan the address space
 * looking for a vma whose range covers vfn. If the page is unmapped,
 * return NULL. */
vmarea_t *
vmmap_lookup(vmmap_t *map, uint32_t vfn)
{
	KASSERT(NULL != map);
    dbg(DBG_PRINT, "(GRADING3A 3.c)\n");
	vmarea_t *temp;
	list_iterate_begin(&map->vmm_list, temp, vmarea_t, vma_plink) {
		if(temp->vma_start <= vfn && temp->vma_end > vfn) return temp;
	} list_iterate_end();
	return NULL;
}

/* Allocates a new vmmap containing a new vmarea for each area in the
 * given map. The areas should have no mmobjs set yet. Returns pointer
 * to the new vmmap on success, NULL on failure. This function is
 * called when implementing fork(2). */
vmmap_t *
vmmap_clone(vmmap_t *map)
{
	vmarea_t *vma;
	vmarea_t *temp;
	dbg(DBG_PRINT,"CODEPATH : Inside vmmap_clone\n");
	vmmap_t *vmmap= vmmap_create();
	if(vmmap == NULL) return NULL;
	list_iterate_begin(&map->vmm_list, temp, vmarea_t, vma_plink) {
		dbg(DBG_PRINT,"CODEPATH(2): Inside vmmap_clone, iterating through vmm_list\n");
		vma = vmarea_alloc();
		if(vma != NULL){
			vma->vma_prot = temp->vma_prot;
			vma->vma_flags = temp->vma_flags;
			vma->vma_start = temp->vma_start;
			vma->vma_off = temp->vma_off;
			vma->vma_end = temp->vma_end;
			vma->vma_obj = temp->vma_obj;
			vma->vma_obj->mmo_ops->ref(vma->vma_obj);
			list_init(&vma->vma_plink);
			list_init(&vma->vma_olink);
			vmmap_insert(vmmap, vma);
		}
		else{
			vmmap_destroy(map);
			return NULL;
		}
	}
	list_iterate_end();
	return vmmap;
}

/* Insert a mapping into the map starting at lopage for npages pages.
 * If lopage is zero, we will find a range of virtual addresses in the
 * process that is big enough, by using vmmap_find_range with the same
 * dir argument.  If lopage is non-zero and the specified region
 * contains another mapping that mapping should be unmapped.
 *
 * If file is NULL an anon mmobj will be used to create a mapping
 * of 0's.  If file is non-null that vnode's file will be mapped in
 * for the given range.  Use the vnode's mmap operation to get the
 * mmobj for the file; do not assume it is file->vn_obj. Make sure all
 * of the area's fields except for vma_obj have been set before
 * calling mmap.
 *
 * If MAP_PRIVATE is specified set up a shadow object for the mmobj.
 *
 * All of the input to this function should be valid (KASSERT!).
 * See mmap(2) for for description of legal input.
 * Note that off should be page aligned.
 *
 * Be very careful about the order operations are performed in here. Some
 * operation are impossible to undo and should be saved until there
 * is no chance of failure.
 *
 * If 'new' is non-NULL a pointer to the new vmarea_t should be stored in it.
 */
int
vmmap_map(vmmap_t *map, vnode_t *file, uint32_t lopage, uint32_t npages,
		int prot, int flags, off_t off, int dir, vmarea_t **new)
{
	int find_range;
	KASSERT(NULL != map);
    dbg(DBG_PRINT, "(GRADING3A 3.d)\n");
    KASSERT(0 < npages);
	dbg(DBG_PRINT, "(GRADING3A 3.d)\n");
    KASSERT((MAP_SHARED & flags) || (MAP_PRIVATE & flags));
    dbg(DBG_PRINT, "(GRADING3A 3.d)\n");
	KASSERT((0 == lopage) || (ADDR_TO_PN(USER_MEM_LOW) <= lopage));
    dbg(DBG_PRINT, "(GRADING3A 3.d)\n");
	KASSERT((0 == lopage) || (ADDR_TO_PN(USER_MEM_HIGH) >= (lopage + npages)));
    dbg(DBG_PRINT, "(GRADING3A 3.d)\n");
	KASSERT(PAGE_ALIGNED(off));
	dbg(DBG_PRINT, "(GRADING3A 3.d)\n");

	vmarea_t *vma = vmarea_alloc();
	if(lopage == 0)
	{
		if((find_range=vmmap_find_range(map,npages,dir))<0) return -ENOMEM;
	}
	else{
		if(vmmap_is_range_empty(map,lopage,npages)){
			find_range=lopage;
		}
		else{
			vmmap_remove(map,lopage,npages);
			find_range=lopage;
		}
	}

	vma->vma_start = find_range;
	vma->vma_flags = flags;
	vma->vma_end = find_range + npages;
	vma->vma_prot = prot;
	vma->vma_off = ADDR_TO_PN(off);
	list_link_init(&vma->vma_olink);

	if(file == NULL) vma->vma_obj=anon_create();
	else if(MAP_ANON & flags) vma->vma_obj=anon_create();
	else
	{
		int error;
		mmobj_t *tempo;
		if((error=file->vn_ops->mmap(file,vma,&(tempo)))<0) return error;
		vma->vma_obj=tempo;
	}
	mmobj_t *temp;
	if(vma->vma_flags & MAP_PRIVATE)
	{
		temp = shadow_create();
		temp->mmo_un.mmo_bottom_obj = mmobj_bottom_obj(vma->vma_obj);
		temp->mmo_shadowed = vma->vma_obj;
		list_insert_head(&vma->vma_obj->mmo_un.mmo_vmas,&vma->vma_olink);
		vma->vma_obj = temp;
	}
	vmmap_insert(map,vma);
	if(new != NULL) *new = vma;
	return 0;
}

/*
 * We have no guarantee that the region of the address space being
 * unmapped will play nicely with our list of vmareas.
 *
 * You must iterate over each vmarea that is partially or wholly covered
 * by the address range [addr ... addr+len). The vm-area will fall into one
 * of four cases, as illustrated below:
 *
 * key:
 *          [             ]   Existing VM Area
 *        *******             Region to be unmapped
 *
 * Case 1:  [   ******    ]
 * The region to be unmapped lies completely inside the vmarea. We need to
 * split the old vmarea into two vmareas. be sure to increment the
 * reference count to the file associated with the vmarea.
 *
 * Case 2:  [      *******]**
 * The region overlaps the end of the vmarea. Just shorten the length of
 * the mapping.
 *
 * Case 3: *[*****        ]
 * The region overlaps the beginning of the vmarea. Move the beginning of
 * the mapping (remember to update vma_off), and shorten its length.
 *
 * Case 4: *[*************]**
 * The region completely contains the vmarea. Remove the vmarea from the
 * list.
 */
int
vmmap_remove(vmmap_t *map, uint32_t lopage, uint32_t npages)
{
	dbg(DBG_PRINT,"CODEPATH : Inside vmmap_remove\n");
	uint32_t old_end;
	vmarea_t *nvma;
	vmarea_t *temp;
	list_iterate_begin(&map->vmm_list, temp, vmarea_t, vma_plink) {
		if (temp->vma_start >= (lopage + npages) || temp->vma_end <= lopage)  continue;
		if((temp->vma_start< lopage) && (temp->vma_end > lopage+npages)){
			old_end=temp->vma_end;
			temp->vma_end = lopage;
			nvma = (vmarea_t *)slab_obj_alloc(vmarea_allocator);

			nvma->vma_off = lopage + npages - temp->vma_start + temp->vma_off;
			nvma->vma_start = lopage+npages;
			nvma->vma_end = old_end;
			nvma->vma_flags = temp->vma_flags;
			nvma->vma_vmmap = temp->vma_vmmap;
			nvma->vma_prot = temp->vma_prot;
			nvma->vma_obj = temp->vma_obj;
			nvma->vma_obj->mmo_ops->ref(nvma->vma_obj);
			list_init(&nvma->vma_plink);
			list_init(&nvma->vma_olink);
			mmobj_t *bottom_obj = mmobj_bottom_obj(temp->vma_obj);
			if (bottom_obj != temp->vma_obj)  list_insert_head(&bottom_obj->mmo_un.mmo_vmas, &nvma->vma_olink);
			list_link_t *prev = &temp->vma_plink;
			list_link_t *next = &nvma->vma_plink;
			next->l_prev = prev;
			prev->l_next->l_prev = next;
			next->l_next = prev->l_next;
			prev->l_next = next;
			continue;
		}
		else if((temp->vma_start < lopage) && (temp->vma_end <= lopage+npages)){
			temp->vma_end = lopage;
			continue;
		}
		else if((temp->vma_start >= lopage) && (temp->vma_end <= lopage+npages)){
			if (list_link_is_linked(&temp->vma_olink)) list_remove(&temp->vma_olink);
			list_remove(&temp->vma_plink);
			temp->vma_obj->mmo_ops->put(temp->vma_obj);
			vmarea_free(temp);
			continue;
		}
		else if((temp->vma_start >= lopage) && (temp->vma_end > lopage+npages)){
			temp->vma_off += npages-(temp->vma_start - lopage);
			temp->vma_start=lopage + npages;
			continue;
		}
	}
	list_iterate_end();
	return 0;
}

/*
 * Returns 1 if the given address space has no mappings for the
 * given range, 0 otherwise.
 */
int
vmmap_is_range_empty(vmmap_t *map, uint32_t startvfn, uint32_t npages)
{
	vmarea_t *temp;
	uint32_t endvfn=startvfn+npages;
	KASSERT((startvfn < endvfn) && (ADDR_TO_PN(USER_MEM_LOW) <= startvfn) && (ADDR_TO_PN(USER_MEM_HIGH) >= endvfn));
	dbg(DBG_PRINT, "(GRADING3A 3.e)\n");
	list_iterate_begin(&map->vmm_list, temp, vmarea_t, vma_plink) {
		if(((temp->vma_start <= startvfn) && (temp->vma_end > startvfn)))return 0;
		else if(((temp->vma_start < endvfn) && (temp->vma_end >= endvfn))) return 0;
		else if(startvfn <= temp->vma_start && temp->vma_end <= endvfn) return 0;
	} list_iterate_end();
	return 1;
}
/* Read into 'buf' from the virtual address space of 'map' starting at
 * 'vaddr' for size 'count'. To do so, you will want to find the vmareas
 * to read from, then find the pframes within those vmareas corresponding
 * to the virtual addresses you want to read, and then read from the
 * physical memory that pframe points to. You should not check permissions
 * of the areas. Assume (KASSERT) that all the areas you are accessing exist.
 * Returns 0 on success, -errno on error.
 */
int
vmmap_read(vmmap_t *map, const void *vaddr, void *buf, size_t count)
{
	dbg(DBG_PRINT,"CODEPATH(1): Inside vmmap_read\n");
	vmarea_t *temp_vmarea;
	uint32_t offset;
	uint32_t pagenum;
	uint32_t phy_pagenum;
	pframe_t *result;
	void *phy_address;
	pagenum = ADDR_TO_PN(vaddr);
	offset = PAGE_OFFSET(vaddr);
	temp_vmarea = vmmap_lookup(map, pagenum);
	phy_pagenum = pagenum - (temp_vmarea->vma_start - temp_vmarea->vma_off);
	pframe_get(temp_vmarea->vma_obj, phy_pagenum, &result);
	phy_address = (void *)((uint32_t)result->pf_addr + offset);
	memcpy(buf,phy_address,count);
	return 0;
}
/* Write from 'buf' into the virtual address space of 'map' starting at
 * 'vaddr' for size 'count'. To do this, you will need to find the correct
 * vmareas to write into, then find the correct pframes within those vmareas,
 * and finally write into the physical addresses that those pframes correspond
 * to. You should not check permissions of the areas you use. Assume (KASSERT)
 * that all the areas you are accessing exist. Remember to dirty pages!
 * Returns 0 on success, -errno on error.
 */
int
vmmap_write(vmmap_t *map, void *vaddr, const void *buf, size_t count)
{
	dbg(DBG_PRINT,"CODEPATH(1): Inside vmmap_write\n");
	vmarea_t *temp_vmarea;
	uint32_t offset;
	uint32_t pagenum;
	uint32_t phy_pagenum;
	pframe_t *result;
	void *phy_address;
	pagenum = ADDR_TO_PN(vaddr);
	offset = PAGE_OFFSET(vaddr);
	temp_vmarea = vmmap_lookup(map, pagenum);
	phy_pagenum = pagenum - (temp_vmarea->vma_start - temp_vmarea->vma_off);
	pframe_get(temp_vmarea->vma_obj, phy_pagenum, &result);
	phy_address = (void *)((uint32_t)result->pf_addr + offset);
	memcpy(phy_address,buf,count);
	return 0;
}

