/******************************************************************************/
/* Important Fall 2016 CSCI 402 usage information:                            */
/*                                                                            */
/* This fils is part of CSCI 402 kernel programming assignments at USC.       */
/*         53616c7465645f5f2e8d450c0c5851acd538befe33744efca0f1c4f9fb5f       */
/*         3c8feabc561a99e53d4d21951738da923cd1c7bbd11b30a1afb11172f80b       */
/*         984b1acfbbf8fae6ea57e0583d2610a618379293cb1de8e1e9d07e6287e8       */
/*         de7e82f3d48866aa2009b599e92c852f7dbf7a6e573f1c7228ca34b9f368       */
/*         faaef0c0fcf294cb                                                   */
/* Please understand that you are NOT permitted to distribute or publically   */
/*         display a copy of this file (or ANY PART of it) for any reason.    */
/* If anyone (including your prospective employer) asks you to post the code, */
/*         you must inform them that you do NOT have permissions to do so.    */
/* You are also NOT permitted to remove or alter this comment block.          */
/* If this comment block is removed or altered in a submitted file, 20 points */
/*         will be deducted.                                                  */
/******************************************************************************/

#include "types.h"
#include "globals.h"
#include "kernel.h"
#include "errno.h"

#include "util/debug.h"

#include "proc/proc.h"

#include "mm/mm.h"
#include "mm/mman.h"
#include "mm/page.h"
#include "mm/mmobj.h"
#include "mm/pframe.h"
#include "mm/pagetable.h"

#include "vm/pagefault.h"
#include "vm/vmmap.h"

/*
 * This gets called by _pt_fault_handler in mm/pagetable.c The
 * calling function has already done a lot of error checking for
 * us. In particular it has checked that we are not page faulting
 * while in kernel mode. Make sure you understand why an
 * unexpected page fault in kernel mode is bad in Weenix. You
 * should probably read the _pt_fault_handler function to get a
 * sense of what it is doing.
 *
 * Before you can do anything you need to find the vmarea that
 * contains the address that was faulted on. Make sure to check
 * the permissions on the area to see if the process has
 * permission to do [cause]. If either of these checks does not
 * pass kill the offending process, setting its exit status to
 * EFAULT (normally we would send the SIGSEGV signal, however
 * Weenix does not support signals).
 *
 * Now it is time to find the correct page (don't forget
 * about shadow objects, especially copy-on-write magic!). Make
 * sure that if the user writes to the page it will be handled
 * correctly.
 *
 * Finally call pt_map to have the new mapping placed into the
 * appropriate page table.
 *
 * @param vaddr the address that was accessed to cause the fault
 *
 * @param cause this is the type of operation on the memory
 *              address which caused the fault, possible values
 *              can be found in pagefault.h
 */
void
handle_pagefault(uintptr_t vaddr, uint32_t cause)
{
	uint32_t page_number = ADDR_TO_PN(vaddr);
	vmarea_t *temp = vmmap_lookup(curproc->p_vmmap, page_number);
	if(temp == NULL){
		curproc->p_status = EFAULT;
		proc_kill(curproc, curproc->p_status);
		return;
	}
	if(temp->vma_prot == PROT_NONE){
		curproc->p_status = EFAULT;
		proc_kill(curproc, curproc->p_status);
		return;
	}
	if(((cause & FAULT_WRITE) && (!(temp->vma_prot & PROT_WRITE)))){
		curproc->p_status = EFAULT;
		proc_kill(curproc, curproc->p_status);
		return;
	}
	if ((!(cause & FAULT_WRITE)) && (!(temp->vma_prot & PROT_READ))) do_exit(EFAULT);
	int write_flag = 0;
	if(cause & FAULT_WRITE)	write_flag = 1;
	pframe_t *lookup_value;
	if((pframe_lookup(temp->vma_obj, ADDR_TO_PN(vaddr)-temp->vma_start + temp->vma_off, write_flag, &lookup_value))<0)	do_exit(EFAULT);
	KASSERT(lookup_value);
	dbg(DBG_PRINT, "(GRADING3A 5.a)\n");
	KASSERT(lookup_value->pf_addr);
	dbg(DBG_PRINT, "(GRADING3A 5.a)\n");
	uintptr_t page_ad;
	page_ad = pt_virt_to_phys((uintptr_t) lookup_value->pf_addr);
	if(cause & FAULT_WRITE)
			pt_map(curproc->p_pagedir, (uintptr_t)PAGE_ALIGN_DOWN(vaddr), page_ad, PD_PRESENT|PD_WRITE|PD_USER, PT_PRESENT|PT_WRITE|PT_USER);
	else
			pt_map(curproc->p_pagedir, (uintptr_t)PAGE_ALIGN_DOWN(vaddr), page_ad, PD_PRESENT|PD_USER, PT_PRESENT|PT_USER);
}
