/******************************************************************************/
/* Important Fall 2016 CSCI 402 usage information:                            */
/*                                                                            */
/* This fils is part of CSCI 402 kernel programming assignments at USC.       */
/*         53616c7465645f5f2e8d450c0c5851acd538befe33744efca0f1c4f9fb5f       */
/*         3c8feabc561a99e53d4d21951738da923cd1c7bbd11b30a1afb11172f80b       */
/*         984b1acfbbf8fae6ea57e0583d2610a618379293cb1de8e1e9d07e6287e8       */
/*         de7e82f3d48866aa2009b599e92c852f7dbf7a6e573f1c7228ca34b9f368       */
/*         faaef0c0fcf294cb                                                   */
/* Please understand that you are NOT permitted to distribute or publically   */
/*         display a copy of this file (or ANY PART of it) for any reason.    */
/* If anyone (including your prospective employer) asks you to post the code, */
/*         you must inform them that you do NOT have permissions to do so.    */
/* You are also NOT permitted to remove or alter this comment block.          */
/* If this comment block is removed or altered in a submitted file, 20 points */
/*         will be deducted.                                                  */
/******************************************************************************/

#include "globals.h"
#include "errno.h"
#include "types.h"

#include "mm/mm.h"
#include "mm/tlb.h"
#include "mm/mman.h"
#include "mm/page.h"

#include "proc/proc.h"

#include "util/string.h"
#include "util/debug.h"

#include "fs/vnode.h"
#include "fs/vfs.h"
#include "fs/file.h"

#include "vm/vmmap.h"
#include "vm/mmap.h"

/*
 * This function implements the mmap(2) syscall, but only
 * supports the MAP_SHARED, MAP_PRIVATE, MAP_FIXED, and
 * MAP_ANON flags.
 *
 * Add a mapping to the current process's address space.
 * You need to do some error checking; see the ERRORS section
 * of the manpage for the problems you should anticipate.
 * After error checking most of the work of this function is
 * done by vmmap_map(), but remember to clear the TLB.
 */
int
do_mmap(void *addr, size_t len, int prot, int flags,
        int fd, off_t off, void **ret)
{
	uint32_t temp;
	if(!(len % PAGE_SIZE))
		temp=len / PAGE_SIZE;
	else
		temp=len / PAGE_SIZE + 1;
	if ((flags & MAP_FIXED) && addr == NULL)
		return -EINVAL;
	if(addr != NULL){
		if(!(((uintptr_t)addr + len) > USER_MEM_LOW && ((uintptr_t)addr + len) <= USER_MEM_HIGH))
			if(!((uintptr_t)addr >= USER_MEM_LOW && (uintptr_t)addr < USER_MEM_HIGH))
				return -EINVAL;
	}
	if (sizeof(len) == NULL || len == 0 || (len == (size_t)-1) || !PAGE_ALIGNED(addr) || flags == 0 || flags == MAP_TYPE || !PAGE_ALIGNED(off))
			return -EINVAL;
	if((fd < 0 || fd >= NFILES ) && ((flags & MAP_ANON) == 0))
			return -EBADF;
	file_t *file = NULL;
	vnode_t *node =NULL;
	if((flags & MAP_ANON) == 0){
		if((file = fget(fd)) == NULL) return -EBADF;
		else node = file->f_vnode;
	}
	if((!(curproc->p_files[fd]->f_mode & FMODE_READ) || !(curproc->p_files[fd]->f_mode & FMODE_WRITE))) {
		if((prot & PROT_WRITE)) {
			if(flags == MAP_SHARED){
				fput(file);
				return -EACCES;
			}
		}
	}
	vmarea_t *area;
	int return_value;
	if((return_value=vmmap_map(curproc->p_vmmap, node, ADDR_TO_PN(addr),temp, prot, flags, off,VMMAP_DIR_HILO,&area))<0){
				return return_value;
	}
	if(area == NULL)
		return -1;
	uintptr_t adress;
	if ( addr == NULL)
		adress = (uintptr_t)PN_TO_ADDR(area->vma_start);
	else
		adress=(uintptr_t)addr;
	if (ret)
		*ret = (void *)adress;
	if(file)
		fput(file);
	pagedir_t *pd = pt_get();
	KASSERT(NULL != curproc->p_pagedir);
	dbg(DBG_PRINT, "(GRADING3A 2.a)\n");
	tlb_flush_range(adress,temp);
	pt_unmap_range(pd, adress, adress + (uintptr_t)PN_TO_ADDR(temp));
	return 0;
}


/*
 * This function implements the munmap(2) syscall.
 *
 * As with do_mmap() it should perform the required error checking,
 * before calling upon vmmap_remove() to do most of the work.
 * Remember to clear the TLB.
 */
int
do_munmap(void *addr, size_t len)
{

	uint32_t temp;
	if(!(len % PAGE_SIZE)){
		temp=len / PAGE_SIZE;
	}
	else{
		temp=len / PAGE_SIZE + 1;
	}
	if(!(((uintptr_t)addr + len) > USER_MEM_LOW && ((uintptr_t)addr + len) <= USER_MEM_HIGH)){
		if(!((uintptr_t)addr >= USER_MEM_LOW && (uintptr_t)addr < USER_MEM_HIGH)){
			return -EINVAL;
		}
	}
	if(!((uintptr_t)addr >= USER_MEM_LOW && (uintptr_t)addr < USER_MEM_HIGH) && !(((uintptr_t)addr + len) > USER_MEM_LOW && ((uintptr_t)addr + len) <= USER_MEM_HIGH)) {
		return -EINVAL;
	}
	if((len == 0 || len == (size_t)-1)){
		return -EINVAL;
	}
	pagedir_t *temp_pd = pt_get();
	vmmap_remove(curproc->p_vmmap,ADDR_TO_PN(addr),(uint32_t)temp);
	tlb_flush_range((uintptr_t)addr,temp);
	pt_unmap_range(temp_pd, (uintptr_t)addr, (uintptr_t)PN_TO_ADDR(ADDR_TO_PN(addr) + temp));
	return 0;
}

