/******************************************************************************/
/* Important Fall 2016 CSCI 402 usage information:                            */
/*                                                                            */
/* This fils is part of CSCI 402 kernel programming assignments at USC.       */
/*         53616c7465645f5f2e8d450c0c5851acd538befe33744efca0f1c4f9fb5f       */
/*         3c8feabc561a99e53d4d21951738da923cd1c7bbd11b30a1afb11172f80b       */
/*         984b1acfbbf8fae6ea57e0583d2610a618379293cb1de8e1e9d07e6287e8       */
/*         de7e82f3d48866aa2009b599e92c852f7dbf7a6e573f1c7228ca34b9f368       */
/*         faaef0c0fcf294cb                                                   */
/* Please understand that you are NOT permitted to distribute or publically   */
/*         display a copy of this file (or ANY PART of it) for any reason.    */
/* If anyone (including your prospective employer) asks you to post the code, */
/*         you must inform them that you do NOT have permissions to do so.    */
/* You are also NOT permitted to remove or alter this comment block.          */
/* If this comment block is removed or altered in a submitted file, 20 points */
/*         will be deducted.                                                  */
/******************************************************************************/

/*
 *  FILE: vfs_syscall.c
 *  AUTH: mcc | jal
 *  DESC:
 *  DATE: Wed Apr  8 02:46:19 1998
 *  $Id: vfs_syscall.c,v 1.13 2015/12/15 14:38:24 william Exp $
 */

#include "kernel.h"
#include "errno.h"
#include "globals.h"
#include "fs/vfs.h"
#include "fs/file.h"
#include "fs/vnode.h"
#include "fs/vfs_syscall.h"
#include "fs/open.h"
#include "fs/fcntl.h"
#include "fs/lseek.h"
#include "mm/kmalloc.h"
#include "util/string.h"
#include "util/printf.h"
#include "fs/stat.h"
#include "util/debug.h"

/* To read a file:
 *      o fget(fd)
 *      o call its virtual read fs_op
 *      o update f_pos
 *      o fput() it
 *      o return the number of bytes read, or an error
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EBADF
 *        fd is not a valid file descriptor or is not open for reading.
 *      o EISDIR
 *        fd refers to a directory.
 *
 * In all cases, be sure you do not leak file refcounts by returning before
 * you fput() a file that you fget()'ed.
 */
int do_read(int fd, void *buf, size_t nbytes) {
	dbg(DBG_PRINT, "CODEPATH: do_Read is invoked\n");
	if (fd < 0 || curproc->p_files[fd] == NULL || fd >= NFILES) {
		dbg(DBG_PRINT, "CODEPATH:do_read, fd is not a valid file descriptor \n");
		return -EBADF;
	}
	file_t *f;
	f = fget(fd);
	if ((f->f_mode & FMODE_READ) == NULL) {
		dbg(DBG_PRINT, "CODEPATH: do_read, fd is not open for reading\n");
		fput(f);
		return -EBADF;
	}
	if (S_ISDIR(f->f_vnode->vn_mode)) {
		dbg(DBG_PRINT, "CODEPATH: do_read, fd refers to a directory\n");
		fput(f);
		return -EISDIR;
	}
	int n = f->f_vnode->vn_ops->read(f->f_vnode, f->f_pos, buf, nbytes);
	f->f_pos += n;
	fput(f);
	return n;
}

/* Very similar to do_read.  Check f_mode to be sure the file is writable.  If
 * f_mode & FMODE_APPEND, do_lseek() to the end of the file, call the write
 * fs_op, and fput the file.  As always, be mindful of refcount leaks.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EBADF
 *        fd is not a valid file descriptor or is not open for writing.
 */
int do_write(int fd, const void *buf, size_t nbytes) {
	dbg(DBG_PRINT, "CODEPATH: do_write is invoked\n");
	file_t *f;
	int n;
	if (fd < 0 || curproc->p_files[fd] == NULL || fd >= NFILES) {
		dbg(DBG_PRINT, "CODEPATH: do_write, fd is not a valid file descriptor\n");
		return -EBADF;
	}
	f = fget(fd);
	if ((f->f_mode & FMODE_WRITE) != NULL) {

		if ((f->f_mode & FMODE_APPEND) != NULL) {
			dbg(DBG_PRINT, "CODEPATH: do_write, do_lseek invoked from do_write\n");
			do_lseek(fd, 0, SEEK_END);
		}

		n = f->f_vnode->vn_ops->write(f->f_vnode, f->f_pos, buf, nbytes);

		if (n >= 0) {
			f->f_pos += n;
			KASSERT(
					(S_ISCHR(f->f_vnode->vn_mode)) || (S_ISBLK(f->f_vnode->vn_mode)) ||((S_ISREG(f->f_vnode->vn_mode)) && (f->f_pos <= f->f_vnode->vn_len)));
			dbg(DBG_PRINT, "(GRADING2A 3.a)\n");
		}
		fput(f);
		return n;
	} else {
		dbg(DBG_PRINT, "CODEPATH: do_write, fd is not open for writing\n");
		fput(f);
		return -EBADF;
	}
}

/*
 * Zero curproc->p_files[fd], and fput() the file. Return 0 on success
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EBADF
 *        fd isn't a valid open file descriptor.
 */
int do_close(int fd) {
	dbg(DBG_PRINT, "CODEPATH: do_close is invoked\n");
	file_t *f;
	vnode_t *v;
	if (fd < 0 || curproc->p_files[fd] == NULL || fd >= NFILES) {
		dbg(DBG_PRINT, "CODEPATH: do_close, fd isn't a valid open file descriptor\n");
		return -EBADF;
	} else {
		dbg(DBG_PRINT, "CODEPATH: do_close, fd closed\n");
		f = fget(fd);
		fput(f);
		curproc->p_files[fd] = NULL;
		fput(f);
		return 0;
	}
}

/* To dup a file:
 *      o fget(fd) to up fd's refcount
 *      o get_empty_fd()
 *      o point the new fd to the same file_t* as the given fd
 *      o return the new file descriptor
 *
 * Don't fput() the fd unless something goes wrong.  Since we are creating
 * another reference to the file_t*, we want to up the refcount.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EBADF
 *        fd isn't an open file descriptor.
 *      o EMFILE
 *        The process already has the maximum number of file descriptors open
 *        and tried to open a new one.
 */
int do_dup(int fd) {
	dbg(DBG_PRINT, "CODEPATH: do_dup is invoked\n");
	if (fd < 0 || curproc->p_files[fd] == NULL || fd >= NFILES) {
		dbg(DBG_PRINT, "CODEPATH: do_dup, fd isn't a valid open file descriptor\n");
		return -EBADF;
	}
	file_t *f;
	f = fget(fd);
	int fd1 = get_empty_fd(curproc);
	if (fd1 == -EMFILE){
		dbg(DBG_PRINT, "CODEPATH:do_dup,  The process already has the maximum number of file descriptors open and tried to open a new one.\n");
		return -EMFILE;
	}
	curproc->p_files[fd1] = f;
	return fd1;
}

/* Same as do_dup, but insted of using get_empty_fd() to get the new fd,
 * they give it to us in 'nfd'.  If nfd is in use (and not the same as ofd)
 * do_close() it first.  Then return the new file descriptor.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EBADF
 *        ofd isn't an open file descriptor, or nfd is out of the allowed
 *        range for file descriptors.
 */
int do_dup2(int ofd, int nfd) {
	dbg(DBG_PRINT, "CODEPATH: do_dup2 is invoked\n");
	if (ofd < 0 || curproc->p_files[ofd] == NULL || ofd >= NFILES) {
		dbg(DBG_PRINT, "CODEPATH: do_dup2, ofd isn't an open file descriptor\n");
		return -EBADF;
	}
	if (nfd < 0 || nfd >= NFILES) {
		dbg(DBG_PRINT, "CODEPATH: do_dup2, nfd is out of the allowed range for file descriptors\n");
		return -EBADF;
	}
	file_t *f;
	f = fget(ofd);
	if (nfd == ofd) {
		dbg(DBG_PRINT, "CODEPATH: do_dup2, nfd same as ofs, return nfd\n");
		fput(f);
		return nfd;
	} else if (curproc->p_files[nfd] && (nfd != ofd)) {
		dbg(DBG_PRINT, "CODEPATH: do_dup2, nfd is in use and not same as ofd, do_close\n");
		do_close(nfd);
	}
	curproc->p_files[nfd] = f;
	return nfd;
}

/*
 * This routine creates a special file of the type specified by 'mode' at
 * the location specified by 'path'. 'mode' should be one of S_IFCHR or
 * S_IFBLK (you might note that mknod(2) normally allows one to create
 * regular files as well-- for simplicity this is not the case in Weenix).
 * 'devid', as you might expect, is the device identifier of the device
 * that the new special file should represent.
 *
 * You might use a combination of dir_namev, lookup, and the fs-specific
 * mknod (that is, the containing directory's 'mknod' vnode operation).
 * Return the result of the fs-specific mknod, or an error.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EINVAL
 *        mode requested creation of something other than a device special
 *        file.
 *      o EEXIST
 *        path already exists.
 *      o ENOENT
 *        A directory component in path does not exist.
 *      o ENOTDIR
 *        A component used as a directory in path is not, in fact, a directory.
 *      o ENAMETOOLONG
 *        A component of path was too long.
 */
int do_mknod(const char *path, int mode, unsigned devid) {
	dbg(DBG_PRINT, "do_mknod is invoked\n");
	size_t length;
	const char *name;
	vnode_t *res_vnode;
	vnode_t *res_lookup;
	if (mode != S_IFCHR && mode != S_IFBLK){
		dbg(DBG_PRINT, "CODEPATH: do_mknod, mode requested creation of something other than a device special  file.\n");
		return -EINVAL;
	}
	if (strlen(path) > MAXPATHLEN){
		dbg(DBG_PRINT, "CODEPATH: do_mknod, A component of path was too long\n");
		return -ENAMETOOLONG;
	}
	int temp;
	temp = dir_namev(path, &length, &name, NULL, &res_vnode);
	if (temp < 0){
		dbg(DBG_PRINT, "CODEPATH: do_mknod, dir_namev returned an error\n");
		return temp;
	}
	if (res_vnode == NULL){
		dbg(DBG_PRINT, "CODEPATH: do_mknod, A directory component in path does not exist\n");
		return -ENOENT;
	}
	if (lookup(res_vnode, name, length, &res_lookup) == 0) {
		dbg(DBG_PRINT, "CODEPATH: do_mknod, path already exists\n");
		vput(res_lookup);
		vput(res_vnode);
		return -EEXIST;
	}
	KASSERT(NULL != res_vnode->vn_ops->mknod);
	dbg(DBG_PRINT, "(GRADING2A 3.b)\n");
	res_vnode->vn_devid = devid;
	int return_value = res_vnode->vn_ops->mknod(res_vnode, name, length, mode,
			devid);
	vput(res_vnode);
	return return_value;
}

/* Use dir_namev() to find the vnode of the dir we want to make the new
 * directory in.  Then use lookup() to make sure it doesn't already exist.
 * Finally call the dir's mkdir vn_ops. Return what it returns.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EEXIST
 *        path already exists.
 *      o ENOENT
 *        A directory component in path does not exist.
 *      o ENOTDIR
 *        A component used as a directory in path is not, in fact, a directory.
 *      o ENAMETOOLONG
 *        A component of path was too long.
 */
int do_mkdir(const char *path) {
	dbg(DBG_PRINT, "do_mkdir is invoked\n");
	size_t length;
	const char *name;
	vnode_t *res_vnode;
	vnode_t *res_lookup;
	if (strlen(path) > MAXPATHLEN){
		dbg(DBG_PRINT, "CODEPATH: do_mkdir, A component of path was too long\n");
		return -ENAMETOOLONG;
	}
	int temp = dir_namev(path, &length, &name, NULL, &res_vnode);
	if (temp < 0){
		dbg(DBG_PRINT, "CODEPATH: do_mkdir, dir_namev returned an error\n");
		return temp;
	}
	if (res_vnode == NULL){
		dbg(DBG_PRINT, "CODEPATH: do_mkdir, A directory component in path does not exist\n");
		return -ENOENT;
	}
	if (!S_ISDIR(res_vnode->vn_mode)) {
		dbg(DBG_PRINT, "CODEPATH: do_mkdir, A component used as a directory in path is not, in fact, a directory\n");
		vput(res_vnode);
		return -ENOTDIR;
	}
	if (res_vnode == NULL){
		dbg(DBG_PRINT, "CODEPATH: do_mkdir, A directory component in path does not exist\n");
		return -ENOENT;
	}
	if (lookup(res_vnode, name, length, &res_lookup) != 0) {
		KASSERT(NULL != res_vnode->vn_ops->mkdir);
		dbg(DBG_PRINT, "(GRADING2A 3.c)\n");
		int return_value = res_vnode->vn_ops->mkdir(res_vnode, name, length);
		vput(res_vnode);
		return return_value;
	} else {
		dbg(DBG_PRINT, "CODEPATH: do_mkdir, path already exists\n");
		vput(res_vnode);
		vput(res_lookup);
		return -EEXIST;
	}
}

/* Use dir_namev() to find the vnode of the directory containing the dir to be
 * removed. Then call the containing dir's rmdir v_op.  The rmdir v_op will
 * return an error if the dir to be removed does not exist or is not empty, so
 * you don't need to worry about that here. Return the value of the v_op,
 * or an error.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EINVAL
 *        path has "." as its final component.
 *      o ENOTEMPTY
 *        path has ".." as its final component.
 *      o ENOENT
 *        A directory component in path does not exist.
 *      o ENOTDIR
 *        A component used as a directory in path is not, in fact, a directory.
 *      o ENAMETOOLONG
 *        A component of path was too long.
 */
int do_rmdir(const char *path) {
	dbg(DBG_PRINT, "CODEPATH:do_rmdir is invoked\n");
	size_t length;
	const char *name;
	vnode_t *res_vnode;
	if (strlen(path) > MAXPATHLEN){
		dbg(DBG_PRINT, "CODEPATH:do_rmdir, is invoked\n");
		return -ENAMETOOLONG;
	}
	int temp = dir_namev(path, &length, &name, NULL, &res_vnode);
	if (temp < 0){
		dbg(DBG_PRINT, "CODEPATH:do_rmdir, dir_namev returned an error\n");
		return temp;
	}
	if (!S_ISDIR(res_vnode->vn_mode)) {
		dbg(DBG_PRINT, "CODEPATH:do_rmdir, A component used as a directory in path is not, in fact, a directory\n");
		vput(res_vnode);
		return -ENOTDIR;
	}
	if (!strcmp(name, "..")) {
		dbg(DBG_PRINT, "CODEPATH:do_rmdir, path has .. as its final component\n");
		vput(res_vnode);
		return -ENOTEMPTY;
	}
	if (!strcmp(name, ".")) {
		dbg(DBG_PRINT, "CODEPATH:do_rmdir, path has . as its final component\n");
		vput(res_vnode);
		return -EINVAL;
	}
	if (res_vnode == NULL){
		dbg(DBG_PRINT, "CODEPATH:do_rmdir, A directory component in path does not exist.\n");
		return -ENOENT;
	}
	KASSERT(NULL != res_vnode->vn_ops->rmdir);
	dbg(DBG_PRINT, "(GRADING2A 3.d)\n");
	dbg(DBG_PRINT, "(GRADING2A 3.d)\n");
	vput(res_vnode);
	int return_value = res_vnode->vn_ops->rmdir(res_vnode, name, length);
	return return_value;
}

/*
 * Same as do_rmdir, but for files.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EISDIR
 *        path refers to a directory.
 *      o ENOENT
 *        A component in path does not exist.
 *      o ENOTDIR
 *        A component used as a directory in path is not, in fact, a directory.
 *      o ENAMETOOLONG
 *        A component of path was too long.
 */
int do_unlink(const char *path) {
	dbg(DBG_PRINT, "CODEPATH:do_unlink is invoked\n");
	size_t length;
	const char *name;
	vnode_t *res_vnode;
	vnode_t *lookup_vnode;
	int temp;
	if ((temp = dir_namev(path, &length, &name, NULL, &res_vnode)) < 0) {
		dbg(DBG_PRINT, "CODEPATH:do_unlink, dir_namev returned an error\n");
		return temp;
	}
	if ((temp = lookup(res_vnode, name, length, &lookup_vnode)) < 0) {
		dbg(DBG_PRINT, "CODEPATH:do_unlink, lookup returned an error\n");
		vput(res_vnode);
		return temp;
	} else {
		if (lookup_vnode == NULL) {
			dbg(DBG_PRINT, "CODEPATH:do_unlink, A component in path does not exist\n");
			vput(res_vnode);
			return -ENOENT;
		} else if (strlen(path) > MAXPATHLEN) {
			dbg(DBG_PRINT, "CODEPATH:do_unlink, A component of path was too long\n");
			vput(lookup_vnode);
			vput(res_vnode);
			return -ENAMETOOLONG;
		} else if (S_ISDIR(lookup_vnode->vn_mode)) {
			dbg(DBG_PRINT, "CODEPATH:do_unlink, path refers to a directory\n");
			vput(lookup_vnode);
			vput(res_vnode);
			return -EISDIR;
		} else {
			KASSERT(NULL != res_vnode->vn_ops->unlink);
			dbg(DBG_PRINT, "(GRADING2A 3.e)\n");
			dbg(DBG_PRINT, "(GRADING2A 3.e)\n");
			temp = res_vnode->vn_ops->unlink(res_vnode, name, length);
			vput(res_vnode);
			vput(lookup_vnode);
			return temp;
		}
	}
}

/* To link:
 *      o open_namev(from)
 *      o dir_namev(to)
 *      o call the destination dir's (to) link vn_ops.
 *      o return the result of link, or an error
 *
 * Remember to vput the vnodes returned from open_namev and dir_namev.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EEXIST
 *        to already exists.
 *      o ENOENT
 *        A directory component in from or to does not exist.
 *      o ENOTDIR
 *        A component used as a directory in from or to is not, in fact, a
 *        directory.
 *      o ENAMETOOLONG
 *        A component of from or to was too long.
 *      o EISDIR
 *        from is a directory.
 */
int do_link(const char *from, const char *to) {
	vnode_t *vnode_from;
	vnode_t *vnode_to;
	vnode_t *res_temp;
	const char *name_to;
	size_t namelen_to;
	int open_res;
	int namev_res;
	int return_value;
	int lookup_res;
	if(strlen(from) > NAME_LEN) return -ENAMETOOLONG;
	if(strlen(to) > NAME_LEN) return -ENAMETOOLONG;
	if((open_res = open_namev(from, O_CREAT, &vnode_from, NULL))<0){
		vput(vnode_from);
		return open_res;
	}
	else if (S_ISDIR(vnode_from->vn_mode)){
		vput(vnode_from);
		return -EISDIR;
	}
	else if((namev_res = dir_namev(to, &namelen_to, &name_to,NULL, &vnode_to))<0){
		vput(vnode_from);
		vput(vnode_to);
		return namev_res;
	}
	else if (!S_ISDIR(vnode_to->vn_mode)){
		vput(vnode_from);
		vput(vnode_to);
		return -ENOTDIR;
	}
	else if(namelen_to > NAME_LEN){
		vput(vnode_from);
		vput(vnode_to);
		return -ENAMETOOLONG;
	}
	else if((lookup_res = lookup(vnode_to,name_to,namelen_to,&res_temp))<0){
		if(lookup_res==-ENOENT){
			return_value = vnode_to->vn_ops->link(vnode_from,vnode_to,name_to,namelen_to);
			vput(vnode_from);
			vput(vnode_to);
			return return_value;
		}
	}
	if(lookup_res == 0){
		vput(vnode_from);
		vput(vnode_to);
		vput(res_temp);
		return -EEXIST;
	}
	else if (S_ISDIR(res_temp->vn_mode)){
		vput(vnode_from);
		vput(vnode_to);
		vput(res_temp);
		return -EISDIR;
	}
	vput(vnode_from);
	vput(vnode_to);
	vput(res_temp);
	return 0;
}

/*      o link newname to oldname
 *      o unlink oldname
 *      o return the value of unlink, or an error
 *
 * Note that this does not provide the same behavior as the
 * Linux system call (if unlink fails then two links to the
 * file could exist).
 */
int do_rename(const char *oldname, const char *newname)/*this code need to be commented*/
{
	dbg(DBG_PRINT, "do_Rename is invoked\n");
	do_link(oldname, newname);
	return do_unlink(oldname);
	return 0;
}

/* Make the named directory the current process's cwd (current working
 * directory).  Don't forget to down the refcount to the old cwd (vput()) and
 * up the refcount to the new cwd (open_namev() or vget()). Return 0 on
 * success.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o ENOENT
 *        path does not exist.
 *      o ENAMETOOLONG
 *        A component of path was too long.
 *      o ENOTDIR
 *        A component of path is not a directory.
 */
int do_chdir(const char *path) {
	dbg(DBG_PRINT, "CODEPATH: do_chdir is invoked\n");
	size_t length;
	const char *name;
	vnode_t *res_vnode;
	if (strlen(path) > MAXPATHLEN){
		dbg(DBG_PRINT, "CODEPATH: do_chdir ,A component of path was too long\n");
		return -ENAMETOOLONG;
	}
	int temp;
	if ((temp = open_namev(path, NULL, &res_vnode, NULL)) == -ENOENT) {
		dbg(DBG_PRINT, "CODEPATH: do_chdir,  path does not exist\n");
		return temp;
	}
	if (!S_ISDIR(res_vnode->vn_mode)) {
		dbg(DBG_PRINT, "CODEPATH: do_chdir, A component of path is not a directory\n");
		vput(res_vnode);
		return -ENOTDIR;
	}

	vput(curproc->p_cwd);
	curproc->p_cwd = res_vnode;
	return 0;

}

/* Call the readdir fs_op on the given fd, filling in the given dirent_t*.
 * If the readdir fs_op is successful, it will return a positive value which
 * is the number of bytes copied to the dirent_t.  You need to increment the
 * file_t's f_pos by this amount.  As always, be aware of refcounts, check
 * the return value of the fget and the virtual function, and be sure the
 * virtual function exists (is not null) before calling it.
 *
 * Return either 0 or sizeof(dirent_t), or -errno.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EBADF
 *        Invalid file descriptor fd.
 *      o ENOTDIR
 *        File descriptor does not refer to a directory.
 */
int do_getdent(int fd, struct dirent *dirp) {
	dbg(DBG_PRINT, "CODEPATH:do_getdent is invoked\n");
	if (fd < 0 || curproc->p_files[fd] == NULL || fd >= NFILES) {
		dbg(DBG_PRINT, "CODEPATH:do_getdent, Invalid file descriptor fd\n");

		return -EBADF;
	}
	file_t *f;
	f = fget(fd);
	if (!S_ISDIR(f->f_vnode->vn_mode)) {
		dbg(DBG_PRINT, "CODEPATH:do_getdent, File descriptor does not refer to a directory\n");
		fput(f);
		return -ENOTDIR;
	}
	if (f->f_vnode->vn_ops->readdir != NULL) {
		int n = f->f_vnode->vn_ops->readdir(f->f_vnode, f->f_pos, dirp);
		if (n <= 0) {
			dbg(DBG_PRINT, "CODEPATH:do_getdent, return 0\n");
			fput(f);
			return 0;
		} else {
			dbg(DBG_PRINT, "CODEPATH:do_getdent, return size of directory\n");
			f->f_pos += n;
			fput(f);
			return sizeof(dirent_t);
		}
	}
	return 0;
}

/*
 * Modify f_pos according to offset and whence.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o EBADF
 *        fd is not an open file descriptor.
 *      o EINVAL
 *        whence is not one of SEEK_SET, SEEK_CUR, SEEK_END; or the resulting
 *        file offset would be negative.
 */
int do_lseek(int fd, int offset, int whence) {
	dbg(DBG_PRINT, "CODEPATH: do_lseek is invoked\n");
	file_t *f;
	int pos = -1;
	if (fd < 0 || fd >= NFILES) {
		dbg(DBG_PRINT, "CODEPATH: do_lseek, (1)fd is not an open file descriptor\n");
		return -EBADF;
	}
	f = curproc->p_files[fd];
	if (!f || !(f->f_vnode)) {
		dbg(DBG_PRINT, "CODEPATH: do_lseek, (2)fd is not an open file descriptor\n");
		return -EBADF;
	}
	if (whence != SEEK_SET && whence != SEEK_CUR && whence != SEEK_END){
		dbg(DBG_PRINT, "CODEPATH: do_lseek, whence is not one of SEEK_SET, SEEK_CUR, SEEK_END\n");
		return -EINVAL;
	}
	if (whence == SEEK_SET) {
		dbg(DBG_PRINT, "CODEPATH: do_lseek, whence is euqal to SEEK_SET\n");
		pos = offset;
	}
	if (whence == SEEK_CUR) {
		dbg(DBG_PRINT, "CODEPATH: do_lseek, whence is euqal to SEEK_CUR\n");
		pos = f->f_pos + offset;
	}
	if (whence == SEEK_END) {
		dbg(DBG_PRINT, "CODEPATH: do_lseek, whence is euqal to SEEK_END\n");
		pos = f->f_vnode->vn_len + offset;
	}
	if (pos < 0) {
		dbg(DBG_PRINT, "CODEPATH: do_lseek, resulting file offset is negative.\n");
		return -EINVAL;
	} else {
		dbg(DBG_PRINT, "CODEPATH: do_lseek, return file pos\n");
		f->f_pos = pos;
		return (f->f_pos);
	}
}

/*
 * Find the vnode associated with the path, and call the stat() vnode operation.
 *
 * Error cases you must handle for this function at the VFS level:
 *      o ENOENT
 *        A component of path does not exist.
 *      o ENOTDIR
 *        A component of the path prefix of path is not a directory.
 *      o ENAMETOOLONG
 *        A component of path was too long.
 */
int do_stat(const char *path, struct stat *buf) {
	dbg(DBG_PRINT, "CODEPATH: do_stat is invoked\n");
	const char *name;
	size_t length;
	vnode_t *res_vnode, *lookup_vnode;
	if (strlen(path) < 1 || buf == NULL) {
		return -EINVAL;
	}
	if (strlen(path) > MAXPATHLEN){
		return -ENAMETOOLONG;
	}
	int temp = dir_namev(path, &length, &name, NULL, &res_vnode);
	if (res_vnode == NULL){
		return -ENOENT;
	}
	if (!S_ISDIR(res_vnode->vn_mode)){
		return -ENOTDIR;
	}
	if ((temp = lookup(res_vnode, name, length, &lookup_vnode)) < 0) {
		dbg(DBG_PRINT,
				"CODEPATH:(2) Inside do_stat, lookup returned an error\n");
		vput(res_vnode);
		return temp;
	}
	KASSERT(res_vnode->vn_ops->stat);
	dbg(DBG_PRINT, "(GRADING2A 3.f)\n");
	dbg(DBG_PRINT, "(GRADING2A 3.f)\n");
	temp = res_vnode->vn_ops->stat(lookup_vnode, buf);
	vput(res_vnode);
	vput(lookup_vnode);
	return temp;
}

#ifdef __MOUNTING__
/*
 * Implementing this function is not required and strongly discouraged unless
 * you are absolutely sure your Weenix is perfect.
 *
 * This is the syscall entry point into vfs for mounting. You will need to
 * create the fs_t struct and populate its fs_dev and fs_type fields before
 * calling vfs's mountfunc(). mountfunc() will use the fields you populated
 * in order to determine which underlying filesystem's mount function should
 * be run, then it will finish setting up the fs_t struct. At this point you
 * have a fully functioning file system, however it is not mounted on the
 * virtual file system, you will need to call vfs_mount to do this.
 *
 * There are lots of things which can go wrong here. Make sure you have good
 * error handling. Remember the fs_dev and fs_type buffers have limited size
 * so you should not write arbitrary length strings to them.
 */
int
do_mount(const char *source, const char *target, const char *type)
{
	NOT_YET_IMPLEMENTED("MOUNTING: do_mount");
	return -EINVAL;
}

/*
 * Implementing this function is not required and strongly discouraged unless
 * you are absolutley sure your Weenix is perfect.
 *
 * This function delegates all of the real work to vfs_umount. You should not worry
 * about freeing the fs_t struct here, that is done in vfs_umount. All this function
 * does is figure out which file system to pass to vfs_umount and do good error
 * checking.
 */
int
do_umount(const char *target)
{
	NOT_YET_IMPLEMENTED("MOUNTING: do_umount");
	return -EINVAL;
}
#endif
