Documentation for Kernel Assignment 2
=====================================

+------------------------+
| BUILD & RUN (Required) |
+------------------------+

Comments: +100
make clean 
make
./weenix -n
+--------------------+
| GRADING (Required) |
+--------------------+

(A.1) In fs/vnode.c:
    (a) In special_file_read(): 6 out of 6 pts
    (b) In special_file_write(): 6 out of 6 pts

(A.2) In fs/namev.c:
    (a) In lookup(): 6 out of 6 pts
    (b) In dir_namev(): 10 out of 10 pts
    (c) In open_namev(): 2 out of 2 pts

(A.3) In fs/vfs_syscall.c:
    (a) In do_write(): 6 out of 6 pts
    (b) In do_mknod(): 2 out of 2 pts
    (c) In do_mkdir(): 2 out of 2 pts
    (d) In do_rmdir(): 2 out of 2 pts
    (e) In do_unlink(): 2 out of 2 pts
    (f) In do_stat(): 2 out of 2 pts

(B) vfstest: 39 out of 39 pts
    Comments: vfstest runs fine

(C.1) faber_fs_thread_test (3 out of 3 pts)
    What is the kshell command to run faber_fs_thread_test(): faber_fs_test
(C.2) faber_directory_test (2 out of 2 pts)
    What is the kshell command to run faber_directory_test(): faber_directory_test

(D) Self-checks: (10 out of 10 pts)
    Comments: No self checks required(please provide details, add subsections and/or items as needed;
                 or, say that none is needed)

Missing required section(s) in README file (vfs-README.txt): No Missing Sections
Submitted binary file : No binary files submitted
Submitted extra (unmodified) file : No extra files submitted
Wrong file location in submission : No wrong file location in submission
Altered or removed top comment block in a .c file : Didnot alter or removed the top comments
Use dbg_print(...) instead of dbg(DBG_PRINT, ...) : no
Not properly indentify which dbg() printout is for which item in the grading guidelines : proper dbg printouts are used
Cannot compile : compiles fine
Compiler warnings : No compiler warnings
"make clean" : Make clean is working
Useless KASSERT : No useless KASSERTS
Insufficient/Confusing dbg : No insufficent or confusing dbg
Kernel panic : No
Cannot halt kernel cleanly : No

+---------------------------------+
| BUGS / TESTS TO SKIP (Required) |
+---------------------------------+

Is there are any tests in the standard test suite that you know that it's not
working and you don't want the grader to run it at all so you won't get extra
deductions, please list them here.  (Of course, if the grader won't run these
tests, you will not get plus points for them.)

Comments: All tests are working fine

+--------------------------------------+
| CONTRIBUTION FROM MEMBERS (Required) |
+--------------------------------------+

1)  Names and USC e-mail addresses of team members: 
             		 Sunakshi Gujral            <sgujral@usc.edu>
			         Vandhana Kowkuntla         <kowkuntl@usc.edu>
			         Shreyas Kagalavadi Shekhar <kagalava@usc.edu>
			         Bandla Snigdha             <bandla@usc.edu>
2)  Is the following statement correct about your submission (please replace
        "yes" with either "yes" or "no", and if the answer is "no",
        please list percentages for each team member) not applicable  "Each team member
        contributed equally in this assignment": yes

+------------------+
| OTHER (Optional) |
+------------------+

Comments on deviation from spec (you will still lose points, but it's better to let the grader know): no deviations from spec
General comments on design decisions: followed the spec 
